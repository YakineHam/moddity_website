const nav = document.querySelector(".nav");
const menuToggle = document.querySelector(".nav__burger");
const filterButton = document.querySelector(".filter-button");
const filterCloseButton = document.querySelector(".filter-menu-closeBtn");
const filterResultButton = document.querySelector(".filter-menu-resultBtn");
const logo = document.querySelector(".nav__logo-img");
const menuMobile = document.querySelector(".menuMobile");
const filterMenu = document.querySelector(".filter-menu");
const stickySubnav = document.querySelector(".sticky-subnav");
const body = document.querySelector("body");
let isTablet = window.innerWidth < 1200 ? true : false;
let isMobile = window.innerWidth < 768 ? true : false;

const openMenu = (button, menu) => {
  button.classList.toggle("is-open");
  menu.classList.toggle("is-open");
};

if (menuToggle && menuMobile) {
  menuToggle.addEventListener("click", () => {
    openMenu(menuToggle, menuMobile);
  });
}

if (filterButton && filterMenu) {
  filterButton.addEventListener("click", () => {
    openMenu(filterButton, filterMenu);
  });
}

if (filterCloseButton && filterMenu) {
  filterCloseButton.addEventListener("click", () => {
    openMenu(filterCloseButton, filterMenu);
  });
}

if (filterResultButton && filterMenu) {
  filterResultButton.addEventListener("click", () => {
    openMenu(filterResultButton, filterMenu);
  });
}

if (window && nav && isTablet && logo && window.innerWidth < 1200) {
  window.addEventListener("scroll", (e) => {
    if (window.scrollY > nav.offsetHeight) {
      nav.classList.add("is-sticky");
      logo.setAttribute("src", "../images/logo-ho.svg");
      if (menuMobile) menuMobile.style.top = nav.offsetHeight;
      if (stickySubnav) stickySubnav.style.top = nav.offsetHeight;
      if (filterMenu) filterMenu.style.top = nav.offsetHeight;
      if (logo) logo.style.width = "76px";
    } else if (window.scrollY < 50) {
      nav.classList.remove("is-sticky");
      logo.setAttribute("src", "../images/logo.svg");
      if (menuMobile) menuMobile.style.top = nav.offsetHeight;
      if (stickySubnav) stickySubnav.style.top = nav.offsetHeight;
      if (filterMenu) filterMenu.style.top = nav.offsetHeight;
      if (logo) logo.style.width = "51px";
    }
  });
}

// Selector component
const buttons = document.querySelectorAll(".button-select");
const selectorElement = document.querySelectorAll(".selectorElement");
const closeSelectorsButton = document.querySelectorAll(".closeSelectors");

if (buttons)
  buttons.forEach((button) => {
    button.addEventListener("click", (e) => {
      const currentButton = e.currentTarget;
      buttons.forEach((elem) => {
        if (currentButton === elem) {
          elem.classList.toggle("is-active");
        } else {
          elem.classList.remove("is-active");
        }
      });
      const id = button.getAttribute("data-id");
      selectorElement.forEach((element) => {
        if (id === element.id) {
          element.classList.toggle("is-active");
        } else {
          element.classList.remove("is-active");
        }
      });
    });
  });

const closeSelectors = () => {
  buttons.forEach((button) => {
    button.classList.remove("is-active");
  });
  selectorElement.forEach((elem) => {
    elem.classList.remove("is-active");
  });
};

if (closeSelectorsButton)
  closeSelectorsButton.forEach((elem) => {
    console.log(elem);
    elem.addEventListener("click", closeSelectors);
  });

const wrapperOverview = document.querySelectorAll(".wrapper-overview");

if (wrapperOverview && !isMobile) {
  wrapperOverview.forEach((wrapper) => {
    const overviewCards = wrapper.querySelectorAll(".overview-card");
    overviewCards.forEach((e) => {
      e.addEventListener("click", (e) => {
        const currentCard = e.currentTarget;
        overviewCards.forEach((card, i) => {
          if (currentCard === card) {
            const overviewSeeMore = card.querySelector(".overviewSeeMore");
            if (overviewSeeMore.contains(e.target)) {
              return;
            }
            let offset = isTablet ? i % 4 : i % 5;
            overviewSeeMore.style.left = `calc((-100% - 24px)*${offset})`;
            card.classList.toggle("is-open");
          } else {
            const overviewSeeMore = card.querySelector(".overviewSeeMore");
            if (overviewSeeMore.contains(e.target)) {
              return;
            }
            card.classList.remove("is-open");
          }
        });
      });
    });
  });
}

// Selectors items

const selectorsContainer = document.querySelector(".selectorsAdded");
const list = document.getElementById("selectorsList");
const removeButton = document.querySelector(".removeAllElements");
const checkboxes = document.querySelectorAll(".checkbox-container");

const selectorElements = [];

checkboxes.forEach((checkbox) => {
  checkbox.addEventListener("click", (e) => {
    e.stopPropagation();
    e.preventDefault();
    const checkInput = e.currentTarget.querySelector("input");
    const text = e.currentTarget.querySelector(".checkmark").innerHTML;
    if (!checkInput.checked) {
      addSelector(text);
      checkInput.checked = true;
    } else {
      checkInput.checked = false;
    }
  });
});

const addSelector = (text) => {
  const html = `<p class="selectorItem">${text}<button class="selectorItem-close" onclick='removeElement(this)'><svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M2 2L15 15" stroke="currentColor" stroke-width="3" stroke-linecap="round"/><path d="M2 15L15 2" stroke="currentColor" stroke-width="3" stroke-linecap="round" /></svg></button></p>`;
  selectorElements.push(text);
  selectorsContainer.style.display = "flex";
  const li = document.createElement("li");
  li.innerHTML = html;
  list.appendChild(li);
};

const removeElement = (e) => {
  e.parentElement.parentElement.remove();
  if (!list.hasChildNodes()) {
    selectorsContainer.style.display = "none";
  }
};

removeButton &&
  removeButton.addEventListener("click", () => {
    removeAllElements();
  });

const removeAllElements = (e) => {
  while (list.firstChild) {
    list.removeChild(list.lastChild);
  }
  selectorsContainer.style.display = "none";
};

const socialsPopinButton = document.querySelector(".socialsPopinButton");
const popinSocials = document.querySelector(".popinSocials");

if (socialsPopinButton)
  socialsPopinButton.addEventListener("click", () => {
    popinSocials.classList.toggle("is-open");
  });

// SwiperJS
// document.addEventListener("load", () => {
const eventSwiper = new Swiper(".mySwiper", {
  slidesPerView: 1.2,
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 24,
    },
    1200: {
      slidesPerView: 3,
    },
  },
  pagination: {
    el: ".swiper-pagination-events",
    type: "progressbar",
  },
  navigation: {
    nextEl: ".button-next-event",
    prevEl: ".button-prev-event",
  },
});

const brandSwiper = new Swiper(".brandsSwiper", {
  slidesPerView: 1.3,
  slidesPerColumn: 2,
  slidesPerColumnFill: "row",
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 4,
      slidesPerColumn: 2,
      slidesPerColumnFill: "row",
      spaceBetween: 24,
    },
    1200: {
      slidesPerView: 6,
      slidesPerColumn: 2,
      slidesPerColumnFill: "row",
      spaceBetween: 24,
    },
  },
  pagination: {
    el: ".swiper-pagination-brands",
    type: "progressbar",
  },
  navigation: {
    nextEl: ".button-next-brand",
    prevEl: ".button-prev-brand",
  },
});

const contactSwiper = new Swiper(".contactsSwiper", {
  slidesPerView: 1.3,
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 24,
    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 24,
    },
  },
  navigation: {
    nextEl: ".button-next-contact",
    prevEl: ".button-prev-contact",
  },
});

const section5Swiper = new Swiper(".section5", {
  slidesPerView: 1,
  navigation: {
    nextEl: ".button-next-section5",
    prevEl: ".button-prev-section5",
  },
});

const section6Swiper = new Swiper(".section6Swiper", {
  slidesPerView: 1.2,
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 2,

      spaceBetween: 24,
    },
  },
  navigation: {
    nextEl: ".button-next-section6",
    prevEl: ".button-prev-section6",
  },
});
const section6BisSwiper = new Swiper(".section6BisSwiper", {
  slidesPerView: 1.2,
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 24,
    },
  },
  navigation: {
    nextEl: ".button-next-section6Bis",
    prevEl: ".button-prev-section6Bis",
  },
});

const section8Swiper = new Swiper(".section8Swiper", {
  slidesPerView: 1.2,
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 24,
    },
    1200: {
      slidesPerView: 3,
    },
  },
  pagination: {
    el: ".swiper-pagination-section8",
    type: "progressbar",
  },
  navigation: {
    nextEl: ".button-next-section8",
    prevEl: ".button-prev-section8",
  },
});
// });

// header anchor nav
const navArticlesElements = document.querySelectorAll(".nav-articles a");

for (const navArticlesElement of navArticlesElements) {
  navArticlesElement.addEventListener("click", (e) => {
    e.preventDefault();
    navArticlesElements.forEach((elem) => {
      elem.classList.remove("active");
    });
    const section = document.querySelector(navArticlesElement.dataset.href);
    console.log(section.offsetTop);
    console.log(nav.offsetHeight);
    // window.scrollTo(0, section.offsetTop - nav.offsetHeight);
    window.scrollTo({
      top: section.offsetTop - nav.offsetHeight,
      left: 0,
      behavior: "smooth",
    });
    navArticlesElement.classList.add("active");
  });
}

// INFINITE SCROLL
// we will add this content, replace for anything you want to add
var more = `<div class="overview-card white">
              <div class="overviewContainer">
                <div class="overviewContainer-text">
                  <p class="title">Title</p>
                  <p class="subtitle">SubTitle</p>
                </div>
              </div>
              <div class="overviewFooter">
                <p>More info</p>
                <svg
                  width="16"
                  height="9"
                  class="down-ico"
                  viewBox="0 0 16 9"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M1 1L7.99512 8L14.9902 1"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-miterlimit="10"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </div>
              <div class="overviewSeeMore">
                <h3 class="overviewSeeMore-title h3">Title</h3>
                <div class="overviewSeeMore-flex">
                  <div class="overviewSeeMore-card">
                    <p class="overviewSeeMore-subtitle h3 h3--light">
                      Sep. 27 > Oct. 2
                    </p>
                    <p class="overviewSeeMore-name highlight">Event name</p>
                    <p class="overviewSeeMore-text text">
                      Adresse sur 1 ligne ou 2 lignes
                    </p>
                    <p class="overviewSeeMore-more text text--light">
                      info supplémentaire
                    </p>
                  </div>
                  <div class="overviewSeeMore-card">
                    <p class="overviewSeeMore-subtitle h3 h3--light">
                      Sep. 27 > Oct. 2
                    </p>
                    <p class="overviewSeeMore-name highlight">Event name</p>
                    <p class="overviewSeeMore-text text">
                      Adresse sur 1 ligne ou 2 lignes
                    </p>
                    <p class="overviewSeeMore-more text text--light">
                      info supplémentaire
                    </p>
                  </div>
                  <div class="overviewSeeMore-card">
                    <p class="overviewSeeMore-subtitle h3 h3--light">
                      Sep. 27 > Oct. 2
                    </p>
                    <p class="overviewSeeMore-name highlight">Event name</p>
                    <p class="overviewSeeMore-text text">
                      Adresse sur 1 ligne ou 2 lignes
                    </p>
                    <p class="overviewSeeMore-more text text--light">
                      info supplémentaire
                    </p>
                  </div>
                  <div class="overviewSeeMore-button">
                    <a href="{{ path('articles') }}" class="button is-p is-rounded"
                      >See full profile</a
                    >
                  </div>
                </div>
              </div>
            </div>`;

var containerInfinite = document.getElementById("container-infinite");
var wrapperInfinite = document.getElementById(".wrapper-infinite");
var test = document.getElementById("test");
window.onscroll = function (ev) {
  if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
    //User is currently at the bottom of the page
    addNewItem();
  }
};

function addNewItem() {
  var itemCount = document.getElementById("wrapper-infinite").childElementCount;
  const itemLimit = 10; //total number of items to retrieve
  //retrieve the next list of items from wherever
  var nextTopItems = getNextItemSimulator(itemCount);
  nextTopItems.forEach(function (item) {
    //add the items to your view
    document.getElementById("wrapper-infinite").innerHTML += more;
    document.getElementById("loading-container").style.display = "flex";
  });
  setTimeout(() => {
    document.getElementById("loading-container").style.display = "none";
  }, 1500);
}

function getNextItemSimulator(currentItem) {
  //Just some dummy data to simulate an api response
  const dummyItemCount = 100;
  var dummyItems = [];
  var nextTopDummyItems = [];
  for (i = 1; i <= dummyItemCount; i++) {
    //add to main dummy list
    dummyItems.push("Movie " + i);
  }
  var countTen = 10;
  var nextItem = currentItem + 1;
  for (i = nextItem; i <= dummyItems.length; i++) {
    //get next 10 records from dummy list
    nextTopDummyItems.push(dummyItems[i - 1]);
    countTen--;
    if (countTen == 0) break;
  }
  return nextTopDummyItems;
}

// BARBA JS
// console.log(barba);
// console.log(barbaCss);
// barba.use(barbaCss);
// barba.init({
//   transition: [
//     {
//       name: "home",
//       afterOnce() {
//         console.log("once");
//       },
//       once() {
//         console.log("once");
//       },
//       beforeOnce() {
//         console.log("once");
//       },
//     },
//   ],
// });
// barba.init({
//   transitions: [
//     {
//       name: "wrapper",
//       leave(data) {
//         return gsap.to(data.current.container, {
//           opacity: 0,
//         });
//       },
//       enter(data) {
//         return gsap.from(data.next.container, {
//           opacity: 0,
//         });
//       },
//     },
//   ],
// });
