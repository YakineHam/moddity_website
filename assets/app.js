/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import Swiper from 'swiper/bundle';

// import styles bundle
import 'swiper/css/bundle';

  // configure Swiper to use modules
  Swiper.use([Navigation, Pagination]);

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';
