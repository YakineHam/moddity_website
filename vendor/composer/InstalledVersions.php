<?php

namespace Composer;

use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '1f1c9eced1987a082eca97886777cc1d9eed4eec',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '1f1c9eced1987a082eca97886777cc1d9eed4eec',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.2',
      'version' => '1.11.99.2',
      'aliases' => 
      array (
      ),
      'reference' => 'c6522afe5540d5fc46675043d3ed5a45a740b27c',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b668aef16090008790395c02c893b1ba13f7e08',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '331b4d5dbaeab3827976273e9356b3b453c300ce',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.8',
      'version' => '1.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1958a744696c6bb3bb0d28db2611dc11610e78af',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d970a11479275300b5144e9373ce5feacfa9b91',
    ),
    'doctrine/data-fixtures' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f18adf13f6c81c67a88360dca359ad474523f8e3',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '96b0053775a544b4a6ab47654dac0621be8b4cf8',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '2.4.3',
      'version' => '2.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '62a188ce2192e6b3b7a2019c26c5001778818b83',
    ),
    'doctrine/doctrine-fixtures-bundle' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '870189619a7770f468ffb0b80925302e065a3b34',
    ),
    'doctrine/doctrine-migrations-bundle' => 
    array (
      'pretty_version' => '3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7ad66566ecce0925786707654df15203782f583a',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1967775546df997eb97057911d80184c91c92b9a',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.10.1',
      'version' => '2.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f346379c7bf39a9f46771cfd9043e0eb2dd98793',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '2.2.2',
      'version' => '2.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ce4712e6dc84a156176a0fbbb11954a25c93103',
    ),
    'doctrine/sql-formatter' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56070bebac6e77230ed7d306ad13528e60732871',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee0db30118f661fb166bcffbf5d82032df484697',
    ),
    'friendsofphp/proxy-manager-lts' => 
    array (
      'pretty_version' => 'v1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '006aa5d32f887a4db4353b13b5b5095613e0611f',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.2',
      'version' => '1.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '848d8125239d7dbf8ab25cb7f054f1a630e68c2e',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '4.4.3',
      'version' => '4.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb324850d09dd437b6acb142c13e64fdc725b0e1',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.26.1',
      'version' => '1.26.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6b00f05152ae2c9b04a448f99c7590beb6042f5',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '50953a2691a922aa1769461637869a0a2faa3f53',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'replaced' => 
      array (
        0 => '^2.1',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'replaced' => 
      array (
        0 => '2.*',
      ),
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '622548b623e81ca6d78b721c5e029f4ce664f170',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a12f7e301eb7258bb68acd89d4aefa05c2906cae',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.14.0',
      'version' => '1.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd86dfc2e2a3cd366cee475e52c6bb3bbc371aa0e',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '9.2.7',
      'version' => '9.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4c798ed8d51506800b441f7a13ecb0f76f12218',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '3.0.5',
      'version' => '3.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa4be8575f26070b100fccb67faabb28f21f66f8',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.5.10',
      'version' => '9.5.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c814a05837f2edb0d1471d6e3f4ab3501ca3899a',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0|2.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '4.0.6',
      'version' => '4.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f4261989e546dc112258c7a75935a81a7ce382',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3461e3fccc7cfdfc2720be910d3bd73c69be590d',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '388b6ced16caa751030f6a69e588299fa09200ac',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '4.0.3',
      'version' => '4.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd89cc98761b8cb5a1a235a6b703ae50d34080e65',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '23bd5951f7ff26f12d4e3242864df3e08dec4e49',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd9d8cf3c5804de4341c283ed787f099f5506172',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '2.3.4',
      'version' => '2.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8cd8a1c753c90bc1a0f5372170e3e489136f914',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '430d14c01836b77c28092883d195a43ce413ee32',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.3.0',
      'version' => '6.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a5d5072dca8f48460fce2f4131fcc495eec654c',
    ),
    'symfony/asset' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '1910a978dbe03503d9ee72408e2fef7c4041d97d',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '9629d1524d8ced5a4ec3e94abdbd638b4ec8319b',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d9074f2777dfde2b78c5c60affa66c5e7518117',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0446463729b89dd4fa62e9aeecc80287323615d',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9ea72de055cd822e5228ff898e2aad2f52f76b0',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a3f7189a0665ee33b50e9e228c46f50f5acbed22',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '5194f18bd80d106f11efa8f7cd0fbdcc3af96ce6',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '43ede438d4cb52cd589ae5dc070e9323866ba8e0',
    ),
    'symfony/debug-bundle' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '858748e3c2fef73187757cb58a62fe7dda61b541',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '75dd7094870feaa5be9ed2b6b1efcfc2b7d3b9b4',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '5129c19ad55f28e52b84954f7e35d842f0134d59',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '4632ae3567746c7e915c33c67a2fb6ab746090c4',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => 'v4.4.29',
      'version' => '4.4.29.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b078eff6268875f4639cf16e48e321982c671bb',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '51f98f7aa99f00f3b1da6bafe934e67ae6ba6dc5',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '2fe81680070043c4c80e7cedceb797e34f377bac',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '78a014771042079cca30716c8471e7a0b985bd22',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '517fb795794faf29086a77d99eb8f35e457837a7',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '70362f1e112280d75b30087c7598b837c1b468b6',
    ),
    'symfony/flex' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '782ef2269622b8349c4bc3dc795fc79d39e8a5b2',
    ),
    'symfony/form' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5aca09fd609cffa1c3470acca2c6f49ae53a5b9',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc936cd733d94e4752a26cfc25a4825724571765',
    ),
    'symfony/http-client' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b900ffa399e25203f30f79f6f4a56b89eee14c2',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e82f6084d7cae521a75ef2cb5c9457bbda785f4',
    ),
    'symfony/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1|2.0',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '09b3202651ab23ac8dcf455284a48a3500e56731',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.32',
      'version' => '4.4.32.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f7bda3ea8f05ae90627400e58af5179b25ce0f38',
    ),
    'symfony/inflector' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '2eb2095edc03a4f0780a417c2cf5b6f6ac5a7284',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '37c8fb0a3d4ccb7df5b9acf432bcf3fe6fbc7675',
    ),
    'symfony/mailer' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edcd1e89670d6b939a8222110ad5e13ab135bd22',
    ),
    'symfony/maker-bundle' => 
    array (
      'pretty_version' => 'v1.34.1',
      'version' => '1.34.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9ae401f3fa2b42881120d33ad79416630d1f2be',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4fd68f54f608c639ddebecfc61746a86134bf4a',
    ),
    'symfony/monolog-bridge' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '9882c03d4c237d77ba5b2845639700653dcd9a84',
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => 'v3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4054b2e940a25195ae15f0a49ab0c51718922eb4',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0b12a3a47ed25749d47d6b4f61412fd5ca1554',
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v5.3.8',
      'version' => '5.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e9c0548d8d7abcd257f18f0adc0517895996a9c1',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a80a521d6176870b6445cfb469c130f9cae1dda',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php56' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php70' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php71' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e66119f3de95efc359483f810c4c3e6436279436',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '13d3161ef63a8ec21eeccaaf9a4d7f784a87a97d',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '727edd3a5fd2feca1562e0f2520ed6888805c0fa',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9955daf3605753c6054ef1dc3ddee993c7ccb5b',
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '7731460a4e192b2377cae0592df4323fc5cd14ea',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ddf033927ad9f30ba2bfd167a7b342cafa13e8e',
    ),
    'symfony/security-bundle' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '49a09063f633d059b34d53c47adee7144c883bbe',
    ),
    'symfony/security-core' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '99ae75e257d5a4fd8537464d98d1dede0180b611',
    ),
    'symfony/security-csrf' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5bba6497d2f1178e23615d5ca01933a29b65a45',
    ),
    'symfony/security-guard' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '68d4be4fe90f4eccbbf379d478f2067550a25469',
    ),
    'symfony/security-http' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebbf7f1c871c1c3c1d54738d0e0f3ae7815a559b',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a10b610f75349dbee1d3ad05c7a2cbf4f1872e34',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c85d997af06a58ba83e2d2538e335b894c24523d',
    ),
    'symfony/swiftmailer-bundle' => 
    array (
      'pretty_version' => 'v3.5.2',
      'version' => '3.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b72355549f02823a2209180f9c035e46ca3f178',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.32',
      'version' => '4.4.32.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db0ba1e85280d8ff11e38d53c70f8814d4d740f5',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '533c37d310d59ab84df8ca6815a581f92e7ffcf6',
    ),
    'symfony/twig-bundle' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebbfcb6977c0fc7fa6def39e48fde66a38125f80',
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '9420a2e8874263a684588283e40252209d80e1a5',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '1f12cc0c2e880a5f39575c19af81438464717839',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae5e31445bef9e27d0999ba2354dc04049508ede',
    ),
    'symfony/web-link' => 
    array (
      'pretty_version' => 'v4.4.27',
      'version' => '4.4.27.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a55c3a0a5da44965f39cf5f770a2e5a4a95c2c68',
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'pretty_version' => 'v4.4.31',
      'version' => '4.4.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '24227617a4ddbdf78f8ab12ce2b76dfb54a7d851',
    ),
    'symfony/webpack-encore-bundle' => 
    array (
      'pretty_version' => 'v1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9943a59f8551b7a8181e19a2b4efa60e5907c667',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.29',
      'version' => '4.4.29.0',
      'aliases' => 
      array (
      ),
      'reference' => '3abcc4db06d4e776825eaa3ed8ad924d5bc7432a',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
    ),
    'twig/extra-bundle' => 
    array (
      'pretty_version' => 'v3.3.3',
      'version' => '3.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa92b8301ff8878e45fe9f54ab7ad99872e080f3',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v3.3.3',
      'version' => '3.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a27fa056df8a6384316288ca8b0fa3a35fdeb569',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
  ),
);







public static function getInstalledPackages()
{
return array_keys(self::$installed['versions']);
}









public static function isInstalled($packageName)
{
return isset(self::$installed['versions'][$packageName]);
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

$ranges = array();
if (isset(self::$installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = self::$installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}





public static function getVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['version'])) {
return null;
}

return self::$installed['versions'][$packageName]['version'];
}





public static function getPrettyVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return self::$installed['versions'][$packageName]['pretty_version'];
}





public static function getReference($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['reference'])) {
return null;
}

return self::$installed['versions'][$packageName]['reference'];
}





public static function getRootPackage()
{
return self::$installed['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
}
}
