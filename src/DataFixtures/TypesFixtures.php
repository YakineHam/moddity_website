<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Collection;
use App\Entity\Product;
use App\Entity\Style;
use App\Entity\SubStyle;
use App\Entity\Internal;
use App\Entity\External;
use App\Entity\CompanySize;
use App\Entity\Land;
use App\Entity\Sourcing;
use App\Entity\Photo;
use App\Entity\SubPlan;
use App\Entity\ProProfile;
use App\Service\TokenService;
use App\Repository\UserRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TypesFixtures extends Fixture
{

    private $encoder;
    private $tokenator;
    private $userRepository;
 
    public function __construct(UserPasswordEncoderInterface $encoder, TokenService $tokenService, UserRepository $userRepository) {
        $this -> encoder = $encoder;
        $this -> tokenator = $tokenService;
        $this -> userRepository = $userRepository;
    }
 
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();

        $types = [
            'Collection' => [
                'Homme',
                'Femme',
                'Unisex',
                'Enfants'
            ],
            'Product' => [
                'Ready to wear',
                'shoes',
                'bags',
                'Eyewear',
                'Headwear',
                'beachwear',
                'jewellery',
                'accessories',
            ],
            'Style' => [
                'Luxury',
                'Contemporary',
                'designer',
            ],
            'SubStyle' => [
                'minimalist',
                'hippie',
                'artisanal',
                'classic',
                'business',
                'street',
                'romantic',
            ],
            'Internal' => [
                'Commercial',
                'Marketing',
                'Design',
                'Atelier',
                'Logistique',
                'Administration',
                'Direction',
                'Communication',
            ],
            'Land' => [
                'European Union',
                'Westerne Europe',
                'Eastern Europe',
                'Scandinavia',
                'Americas',
                'North America',
                'South America',
                'Asia / Pacific',
                'Africa',
                'Oceania',
                'Europe',
                'Italy',
                'France',
                'Germany',
                'WORLD',
            ],
            'Sourcing' => [
                'Fabrics',
                'Yarns',
                'Accessories',
                'Manufacturing' ,
                'Trends',
                'Leather',
            ],
            'CompanySize' => [
                '1/20 employees',
                '20/50 employees',
                '50/100 employes',
                '>100 employees',
            ],
            'External' => [
                'Agent',
                'Distributeur',
                'Representant',
            ]
        ];


        foreach($types as $class => $value){
            foreach($value as $property => $data){
                if($class == 'Collection'){
                    $object = new Collection;
                }
                elseif($class == 'Product'){
                    $object = new Product;
                }
                elseif($class == 'Style'){
                    $object = new Style;
                }
                elseif($class == 'SubStyle'){
                    $object = new SubStyle;
                }
                elseif($class == 'Internal'){
                    $object = new Internal;
                }
                elseif($class == 'External'){
                    $object = new External;
                }
                elseif($class == 'CompanySize'){
                    $object = new CompanySize;
                }
                elseif($class == 'Sourcing'){
                    $object = new Sourcing;
                }
                elseif($class == 'Land'){
                    $object = new Land;
                }
                $object 
                    -> setLabel($data)
                    -> setIsActive(1)
                    -> setCreatedAt(new \DateTime('now'))
                    -> setUpdatedAt(new \DateTime('now'))
                ;
                $manager -> persist($object);
            } 
        }
        




        $manager -> flush();
    }
}
