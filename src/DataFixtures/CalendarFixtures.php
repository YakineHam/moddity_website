<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\FashionWeeks;
use App\Entity\SaisonsMode;
use App\Entity\SalonType;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CalendarFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        // Création fashion weeks
        $pays = ['Londres', 'Paris', 'Milan', 'New York', 'Berlin', 'Francfort', 'Copenhague', 'Londres', 'Paris', 'Milan', 'New York', 'Berlin'];
        $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'octobre', 'novembre', 'decembre'];
        for($i = 0; $i < 12; $i++)
        {
            if($i%2 == 0)
                $bool = true;
            else 
                $bool = false;

            $fashionWeeks = new FashionWeeks;

            $fashionWeeks->setName("Fashion Week $pays[$i] $months[$i] 2022")
                         ->setPlace($pays[$i])
                         ->setStartDate($faker->dateTimeInInterval('-2 months', '+90 days'))
                         ->setEndDate($faker->dateTimeInInterval('-2 months', '+90 days'))
                         ->setSectionOnline($bool)
                         ->setCreationMap($bool);
            
            $manager->persist($fashionWeeks);
        }

        // Création catégories de sessions salons 
        $label = ['Fashion', 'Sourcing', 'Conférences'];
        $choixUnique = ['Men', 'Women', 'Kids'];
        $choixMUlti = ["Men / women / kids", "Ready to Wear / Accessories / Jewellery / Eyewear / Beachwear", "Sourcing / Technology / Business / Innovation / Finance"];

        for($j = 0; $j < 3; $j++)
        {
            if($j%2 == 0)
            {
                $type = 'unique';
                $choix = $choixUnique[$j];
            }
            else
            {
                $ype = 'multi';
                $choix = $choixMUlti[$j];
            }
                
            $salonType = new SalonType;

            $salonType->setLabel($label[$j])
                      ->setType($type)
                      ->setChoix($choix);

            $manager->persist($salonType);
        }

        // Création saisons mode
        for($k = 2; $k < 9; $k++)
        {
            if($k%2 == 0)
                $saison = 'Printemps/été 2023';
            else
                $saison = 'Automne/hiver 2024';

            $saisonMode = new SaisonsMode;

            $saisonMode->setLabel($saison);

            $manager->persist($saisonMode);
        }

        $manager->flush();
    }
}
