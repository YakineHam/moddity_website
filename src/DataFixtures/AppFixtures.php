<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Photo;
use App\Entity\SubPlan;
use App\Entity\ProProfile;
use App\Service\TokenService;
use App\Repository\UserRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;
    private $tokenator;
    private $userRepository;
 
    public function __construct(UserPasswordEncoderInterface $encoder, TokenService $tokenService, UserRepository $userRepository) {
        $this -> encoder = $encoder;
        $this -> tokenator = $tokenService;
        $this -> userRepository = $userRepository;
    }
 
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();


        // $product = new Product();
        // $manager->persist($product);

        /*
         * 
         * ADD USERS 
         * 
         * 
         */

        // VISITOR
        for($i = 0; $i < 100; $i++){
            $user = new User; 
            $user 
                -> setLname($faker -> lastName )
                -> setFname($faker -> firstName)
                -> setEmail($faker -> email)
                -> setUsername($faker -> username)
                -> setRole($faker -> randomElement($array = array ('ROLE_USER','ROLE_CLIENT')))
                -> setPassword($this -> encoder -> encodePassword($user, 'Password01'))
                -> setIsValidated($faker -> boolean($chanceOfGettingTrue = 55))
                -> setIsConfirm($faker -> boolean($chanceOfGettingTrue = 85))
                -> setIsActive($faker -> boolean($chanceOfGettingTrue = 95))
                -> setCreatedAt($faker -> dateTimeThisYear($max = 'now', $timezone = null))
                -> setToken($this -> tokenator -> getRandomToken())
            ;
            $manager -> persist($user); 
        }
        $manager -> flush();

        // ADMIN
        for($j = 1; $j <= 3; $j++){
            $admin = new User; 
            $admin 
                -> setLname($faker -> lastName )
                -> setFname($faker -> firstName)
                -> setEmail($faker -> email)
                -> setUsername('admin_0' . $j)
                -> setRole('ROLE_ADMIN')
                -> setPassword($this -> encoder -> encodePassword($user, 'Password01'))
                -> setIsValidated(true)
                -> setIsConfirm(true)
                -> setIsActive(true)
                -> setCreatedAt(new \DateTime('now'))
                -> setToken($this -> tokenator -> getRandomToken())
            ;
            $manager -> persist($admin); 
        }

        // SUPER ADMIN
        $superAdmin = new User; 
        $superAdmin 
            -> setLname('HAMIDA')
            -> setFname('Yakine')
            -> setEmail('yakine.hamida@gmail.com')
            -> setUsername('Yakine22')
            -> setRole('ROLE_SUPER_ADMIN')
            -> setPassword($this -> encoder -> encodePassword($user, 'Password01'))
            -> setIsValidated(true)
            -> setIsConfirm(true)
            -> setIsActive(true)
            -> setCreatedAt(new \DateTime('now'))
            -> setToken($this -> tokenator -> getRandomToken())
        ;
        $manager -> persist($superAdmin); 


       
        

         /*
         * 
         * ADD SUPLANS
         * 
         * 
         */
        // Plan 0
        $plan0 = new SubPlan();
        $plan0 
            -> setUuid(0)
            -> setLabel('Orphelin')
            -> setPeriod('monthly')
            -> setPrice(00.00)
            -> setIsActive(1)
            -> setIsPaying(0)
        ;
        $manager -> persist($plan0);
        // Plan 1 - Claimed
        $plan1 = new SubPlan();
        $plan1 
            -> setUuid(1)
            -> setLabel('claimed')
            -> setPeriod('yearly')
            -> setPrice(00.00)
            -> setIsActive(1)
            -> setIsPaying(0)
        ;
        $manager -> persist($plan1);

        // Plan 2 - Basic
        $plan2 = new SubPlan();
        $plan2 
            -> setUuid(2)
            -> setLabel('Basic')
            -> setPeriod('monthly')
            -> setPrice(100.00)
            -> setIsActive(1)
            -> setIsPaying(1)
        ;
        $manager -> persist($plan2);

        // Plan 3 - Premium
        $plan3 = new SubPlan();
        $plan3 
            -> setUuid(3)
            -> setLabel('Premium')
            -> setPeriod('monthly')
            -> setPrice(250.00)
            -> setIsActive(1)
            -> setIsPaying(1)
        ;
        $manager -> persist($plan3);

        // Plan 4 - VIP
        $plan4 = new SubPlan();
        $plan4 
            -> setUuid(4)
            -> setLabel('VIP')
            -> setPeriod('monthly')
            -> setPrice(500.00)
            -> setIsActive(1)
            -> setIsPaying(1)
        ;
        $manager -> persist($plan4);

        // Plan 5 - GOLD
        $plan5 = new SubPlan();
        $plan5 
            -> setUuid(5)
            -> setLabel('GOLD')
            -> setPeriod('monthly')
            -> setPrice(500.00)
            -> setIsActive(1)
            -> setIsPaying(1)
        ;
        $manager -> persist($plan5);

        // Plan 6 - ONE SHOT BASIC
        $plan6 = new SubPlan();
        $plan6 
            -> setUuid(6)
            -> setLabel('One Shot - Basic')
            -> setPeriod('oneshot')
            -> setPrice(1500.00)
            -> setIsActive(1)
            -> setIsPaying(1)
        ;
        $manager -> persist($plan6);

        // Plan 7 - ONE SHOT BASIC
        $plan7 = new SubPlan();
        $plan7 
            -> setUuid(7)
            -> setLabel('One Shot - Premium')
            -> setPeriod('oneshot')
            -> setPrice(2500.00)
            -> setIsActive(1)
            -> setIsPaying(1)
        ;
        $manager -> persist($plan7);

        /**
         * 
         * Photo gallerie
         * 
         */
        $photos = [
            'collection-mango-printemps-ete-2021.jpg',
            'robe-longue-selection-ete-2021.jpg',
            'robes-ete-2021.jpg',
            'generated-defile-5f75f5614506f.jpg',
            'COURANT-0.jpg',
            'zara-collection-printemps-ete.jpg',
            '0603565778772-web-tete.jpg',
            'la-redoute-collection-printemps-ete-2021-look-femme-jean-large-blouson-tweed.jpg',
            '1c87aedf-fb49-43fd-b124-e111433f3b50.jpeg',
            'drole-de-monsieur-printemps-ete-2021-collection-lookbook-0.jpg',
        ];

        /* 
        * ADD PROFILE
        * 
        * 
        */
        $users = $this -> userRepository -> findAll();
        $profileType = ['designer','showroom','communication','production','service','boutique'];
        $plans = [$plan0, $plan1, $plan2, $plan3, $plan4];
        for($j = 0; $j < 35; $j++){
            $profile = new ProProfile();
            $profile
            -> setType($faker -> randomElement($profileType))
            -> setLabel($faker -> lastName . ' ' . $faker -> firstName)
            -> setCity($faker -> city)
            -> setZipCode($faker -> postcode )
            -> setEmailMain($faker -> email)
            -> setSubPlan($faker -> randomElement($plans))
            -> setLatitude($faker -> latitude($min = -90, $max = 90))
            -> setLongitude($faker -> longitude($min = -180, $max = 180))
            -> addUser($faker -> randomElement($users))
            -> setToken($this -> tokenator -> getRandomToken())
            -> setShortToken($this -> tokenator -> getSmallRandomToken(10))
            ;

            for($y = 1; $y < rand(1, 12); $y++){
                $photo = new Photo;
                $photo 
                    -> setName($faker -> randomElement($photos))
                    -> setPosition($y)
                ;
                $manager -> persist($photo);
                $profile -> addPhoto($photo);
            }

            $manager -> persist($profile);
        }
        

        

        $manager->flush();
    }
}
