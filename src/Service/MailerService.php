<?php 

namespace App\Service; 

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MailerService
{

    private $emailSender;
    private $templating;
    private $mailer;
    
    /**
     * __construct
     *
     * @return void
     * 
     */
    public function __construct(\Swift_Mailer $mailer, ParameterBagInterface $param,  \Twig\Environment $templating, SessionInterface $session){
        $this -> emailSender = $param -> get('email_sender');
        $this -> templating = $templating;
        $this -> mailer = $mailer ;
       
    }

    public function sendConfirmAdressEmail($user)
    {
        $message = (new \Swift_Message('Confirm your MODDITY account'))
            ->setFrom($this -> emailSender)
            ->setTo($user -> getEmail())
            ->setBody(
                $this -> templating -> render(
                    // templates/emails/registration.html.twig
                    'emails/confirm_address_email.html.twig',
                    [
                        'username' => $user -> getUsername(),
                        'token' => $user -> getToken(),
                    ]
                ),
                'text/html'
            )
        ;

        return $this -> mailer -> send($message);
    }


}