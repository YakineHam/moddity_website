<?php 

namespace App\Service; 

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TokenService
{

    private $privateSalt;

    /**
     * __construct
     *
     * @return void
     * 
     */
    public function __construct(ParameterBagInterface $param, SessionInterface $session){
        $this -> privateSalt = $param -> get('private_salt');
    }



    /**
     * 
     * Function to create a rand token for confirm y email
     *
     */
    public function getRandomToken(){
        $plainToken = rand(1, 999999) . '-' . time();
        $myNumberPickedUp = ['2', '4', '6'];
        $plainToken = str_replace($myNumberPickedUp, $this -> privateSalt, $plainToken);
        $token_argon2i = password_hash($plainToken, PASSWORD_ARGON2I,); 
        $token_tab = explode(',', $token_argon2i);   
        $token = str_replace('p=', '', $token_tab[2]);
        $token_new = str_replace('/', '', $token);
        return $token_new;
    }

    /**
     * 
     * Function to create a rand token for newsletter subscription 
     * to be abale to change the subscription and unsuscribe
     *
     */
    public function getSmallRandomToken($nb){
        $plainToken = rand(1, 999999) . '-' . time();
        $myNumberPickedUp = ['1', '3', '8', '7'];
        $plainToken = str_replace($myNumberPickedUp, $this -> privateSalt, $plainToken);
        $token_argon2i = password_hash($plainToken, PASSWORD_ARGON2I,); 
        $token_tab = explode(',', $token_argon2i);   
        $token = str_replace('p=', '', $token_tab[2]);
        $token_new = str_replace('/', '', $token);
        return substr($token_new, 2, $nb);
    }


}