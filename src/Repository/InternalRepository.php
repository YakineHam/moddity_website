<?php

namespace App\Repository;

use App\Entity\Internal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Internal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Internal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Internal[]    findAll()
 * @method Internal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InternalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Internal::class);
    }

    // /**
    //  * @return Internal[] Returns an array of Internal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Internal
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
