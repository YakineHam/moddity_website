<?php

namespace App\Repository;

use App\Entity\SalonType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SalonType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalonType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalonType[]    findAll()
 * @method SalonType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalonTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalonType::class);
    }

    // /**
    //  * @return SalonType[] Returns an array of SalonType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SalonType
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
