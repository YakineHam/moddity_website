<?php

namespace App\Repository;

use App\Entity\Salons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Salons|null find($id, $lockMode = null, $lockVersion = null)
 * @method Salons|null findOneBy(array $criteria, array $orderBy = null)
 * @method Salons[]    findAll()
 * @method Salons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalonsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Salons::class);
    }

    // /**
    //  * @return Salons[] Returns an array of Salons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Salons
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
