<?php

namespace App\Repository;

use App\Entity\SubStyle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubStyle|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubStyle|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubStyle[]    findAll()
 * @method SubStyle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubStyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubStyle::class);
    }

    // /**
    //  * @return SubStyle[] Returns an array of SubStyle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubStyle
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
