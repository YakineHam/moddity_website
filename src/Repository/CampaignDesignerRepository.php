<?php

namespace App\Repository;

use App\Entity\CampaignDesigner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CampaignDesigner|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampaignDesigner|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampaignDesigner[]    findAll()
 * @method CampaignDesigner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignDesignerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampaignDesigner::class);
    }

    // /**
    //  * @return CampaignDesigner[] Returns an array of CampaignDesigner objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CampaignDesigner
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
