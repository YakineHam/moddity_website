<?php

namespace App\Repository;

use App\Entity\EvenementFashionWeeks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvenementFashionWeeks|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvenementFashionWeeks|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvenementFashionWeeks[]    findAll()
 * @method EvenementFashionWeeks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementFashionWeeksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvenementFashionWeeks::class);
    }

    // /**
    //  * @return EvenementFashionWeeks[] Returns an array of EvenementFashionWeeks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvenementFashionWeeks
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
