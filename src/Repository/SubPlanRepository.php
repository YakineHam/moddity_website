<?php

namespace App\Repository;

use App\Entity\SubPlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubPlan|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubPlan|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubPlan[]    findAll()
 * @method SubPlan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubPlanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubPlan::class);
    }

    // /**
    //  * @return SubPlan[] Returns an array of SubPlan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubPlan
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
