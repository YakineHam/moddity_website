<?php

namespace App\Repository;

use App\Entity\CampaignShowroom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CampaignShowroom|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampaignShowroom|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampaignShowroom[]    findAll()
 * @method CampaignShowroom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignShowroomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampaignShowroom::class);
    }

    // /**
    //  * @return CampaignShowroom[] Returns an array of CampaignShowroom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CampaignShowroom
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
