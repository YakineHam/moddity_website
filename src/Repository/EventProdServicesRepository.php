<?php

namespace App\Repository;

use App\Entity\EventProdServices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventProdServices|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventProdServices|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventProdServices[]    findAll()
 * @method EventProdServices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventProdServicesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventProdServices::class);
    }

    // /**
    //  * @return EventProdServices[] Returns an array of EventProdServices objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventProdServices
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
