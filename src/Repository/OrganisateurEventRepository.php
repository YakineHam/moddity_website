<?php

namespace App\Repository;

use App\Entity\OrganisateurEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganisateurEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganisateurEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganisateurEvent[]    findAll()
 * @method OrganisateurEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganisateurEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganisateurEvent::class);
    }

    // /**
    //  * @return OrganisateurEvent[] Returns an array of OrganisateurEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganisateurEvent
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
