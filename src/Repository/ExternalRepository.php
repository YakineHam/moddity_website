<?php

namespace App\Repository;

use App\Entity\External;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method External|null find($id, $lockMode = null, $lockVersion = null)
 * @method External|null findOneBy(array $criteria, array $orderBy = null)
 * @method External[]    findAll()
 * @method External[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExternalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, External::class);
    }

    // /**
    //  * @return External[] Returns an array of External objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?External
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
