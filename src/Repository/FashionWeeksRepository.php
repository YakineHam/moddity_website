<?php

namespace App\Repository;

use App\Entity\FashionWeeks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FashionWeeks|null find($id, $lockMode = null, $lockVersion = null)
 * @method FashionWeeks|null findOneBy(array $criteria, array $orderBy = null)
 * @method FashionWeeks[]    findAll()
 * @method FashionWeeks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FashionWeeksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FashionWeeks::class);
    }

    // /**
    //  * @return FashionWeeks[] Returns an array of FashionWeeks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FashionWeeks
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
