<?php

namespace App\Repository;

use App\Entity\ProProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProProfile[]    findAll()
 * @method ProProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProProfile::class);
    }

    // /**
    //  * @return ProProfile[] Returns an array of ProProfile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProProfile
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
