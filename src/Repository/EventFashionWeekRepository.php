<?php

namespace App\Repository;

use App\Entity\EventFashionWeek;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventFashionWeek|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventFashionWeek|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventFashionWeek[]    findAll()
 * @method EventFashionWeek[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventFashionWeekRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventFashionWeek::class);
    }

    // /**
    //  * @return EventFashionWeek[] Returns an array of EventFashionWeek objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventFashionWeek
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
