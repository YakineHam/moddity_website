<?php

namespace App\Repository;

use App\Entity\FilActu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FilActu|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilActu|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilActu[]    findAll()
 * @method FilActu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilActuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilActu::class);
    }

    // /**
    //  * @return FilActu[] Returns an array of FilActu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FilActu
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
