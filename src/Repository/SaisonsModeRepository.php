<?php

namespace App\Repository;

use App\Entity\SaisonsMode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SaisonsMode|null find($id, $lockMode = null, $lockVersion = null)
 * @method SaisonsMode|null findOneBy(array $criteria, array $orderBy = null)
 * @method SaisonsMode[]    findAll()
 * @method SaisonsMode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaisonsModeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SaisonsMode::class);
    }

    // /**
    //  * @return SaisonsMode[] Returns an array of SaisonsMode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SaisonsMode
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
