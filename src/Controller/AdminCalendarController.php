<?php

namespace App\Controller;

use App\Entity\Salons;
use App\Entity\FilActu;
use App\Entity\SalonType;
use App\Form\FilActuType;
use App\Form\EventFwsType;
use App\Entity\SaisonsMode;
use App\Form\SalonFormType;
use App\Entity\CategoryActu;
use App\Entity\FashionWeeks;
use App\Form\SalonsFormType;
use App\Form\SaisonsModeType;
use App\Entity\SaisonSourcing;
use App\Form\CategoryActuType;
use App\Form\EvenementFwsType;
use App\Form\FashionWeeksType;
use App\Entity\CampaignDesigner;
use App\Entity\CampaignShowroom;
use App\Entity\EventFashionWeek;
use App\Form\SaisonSourcingType;
use App\Entity\OrganisateurEvent;
use App\Form\CampaignDesignerType;
use App\Form\CampaignShowroomType;
use App\Form\OrganisateurEventType;
use App\Repository\SalonsRepository;
use App\Entity\EvenementFashionWeeks;
use App\Repository\FilActuRepository;
use App\Repository\SalonTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\SaisonsModeRepository;
use App\Repository\CategoryActuRepository;
use App\Repository\FashionWeeksRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CampaignDesignerRepository;
use App\Repository\CampaignShowroomRepository;
use App\Repository\EventFashionWeekRepository;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\OrganisateurEventRepository;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EvenementFashionWeeksRepository;
use App\Repository\SaisonSourcingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class AdminCalendarController extends AbstractController
{
    /**
     * @Route("/admin/calendar", name="admin_calendar")
     */
    public function calendar(SalonsRepository $repoSalons, EvenementFashionWeeksRepository $repoEvenementFws): Response
    {
        $eventsSalons = $repoSalons->findAll();
        $evenementFws = $repoEvenementFws->findAll();
        dump($evenementFws);

        $events = [];
        
        foreach($eventsSalons as $event){
            $events[] = [
                // 'id' => $event->getId(), 
                'title' => 'Salon : ' . $event->getLocation() . "\n" . $event->getAddress() . ' - ' . $event->getCity(),
                'start' => $event->getStartDate()->format('Y-m-d H:i:s'),
                'end' => $event->getEndDate()->format('Y-m-d H:i:s'),
                'className' => 'bg-info'
                // 'location' => $event->getLocation(), 
                // 'address' => $event->getAddress(), 
                // 'city' => $event->getCity(), 
                // 'zip_code' => $event->getZipCode(),
                // 'link_salon_digital' => $event->getLinkSalonDigital(), 
                // 'link_exposants' => $event->getLinkExposants(),  
                // 'publish_online' => $event->getPublishOnline(), 
                // 'publish_map' => $event->getPublishMap()
            ];
        }

        foreach($evenementFws as $event){

            $events[] = [
                // 'id' => $event->getId(), 
                'title' => $event->getFashionWeeks()->getName() . "\n" . $event->getLocation() . "\n" . $event->getOrganisteurEvent()->getLabel() . "\n" . $event->getAdresse() . "\n" .$event->getVille() . '-' . $event->getCodePostal(),
                ($event->getDateUnique() ? 'start' : '') => ($event->getDateUnique() ? $event->getDateUnique()->format('Y-m-d H:i:s') : ''),
                ($event->getDateDebut() ? 'start' : '') => ($event->getDateDebut() ? $event->getDateDebut()->format('Y-m-d H:i:s') : ''),
                ($event->getDateFin() ? 'end' : '') => ($event->getDateFin() ? $event->getDateFin()->format('Y-m-d H:i:s') : ''),
                'className' => 'bg-success'
                // 'location' => $event->getLocation(), 
                // 'address' => $event->getAddress(), 
                // 'city' => $event->getCity(), 
                // 'zip_code' => $event->getZipCode(),
                // 'link_salon_digital' => $event->getLinkSalonDigital(), 
                // 'link_exposants' => $event->getLinkExposants(),  
                // 'publish_online' => $event->getPublishOnline(), 
                // 'publish_map' => $event->getPublishMap()
            ];
        }

        $data = json_encode($events);
        dump($data);

        return $this->render('admin_calendar/calendar.html.twig', compact('data'));
    }

    /**
     * Méthode permettant l'affichage de la table SQL fashion_weeks
     * 
     * @Route("/admin/fashion_weeks", name="admin_fashion_weeks")
     */
    public function adminFashionWeeks(EntityManagerInterface $manager, FashionWeeksRepository $repoFashionWeeks): Response
    {
        $colonnes = $manager->getClassMetadata(FashionWeeks::class)->getFieldNames();
        dump($colonnes);

        $fashionWeeks = $repoFashionWeeks->findAll();
        dump($fashionWeeks);

        return $this->render('admin_calendar/admin_fashion_weeks.html.twig', [
            'colonnes' => $colonnes,
            'fashionWeeks' => $fashionWeeks
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une fashion weeks
     * 
     * @Route("/admin/add_fashion_weeks", name="add_fashion_weeks")
     * @Route("/Admin/update_fashion_weeks/{id}", name="update_fashion_weeks")
     */
    public function adminFormFashionWeeks(Request $request, EntityManagerInterface $manager, FashionWeeks $fashionWeeks = null): Response 
    {
        if (!$fashionWeeks) {
            $fashionWeeks = new FashionWeeks;
        }
        
        $formFashionWeeks = $this->createForm(FashionWeeksType::class, $fashionWeeks);

        $formFashionWeeks->handleRequest($request);

        dump($fashionWeeks);

        if($formFashionWeeks->isSubmitted() && $formFashionWeeks->isValid())
        {
            // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$fashionWeeks->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($fashionWeeks);
            $manager->flush();

            $this->addFlash('success', "La fashion Weeks a été $word avec succés !");

            return $this->redirectToRoute('admin_fashion_weeks');
        }

        return $this->render('admin_calendar/admin_form_fashion_weeks.html.twig', [
            'formFashionWeeks' => $formFashionWeeks->createView(),
            'editMode' => $fashionWeeks->getId()
        ]);
    }

    /**
     * Méthode permettant de supprimer une fashion week de la BDD
     * 
     * @Route("admin/delete_fashion_weeks/{id}", name="delete_fashion_weeks")
     */
    public function adminDeleteFashionWeeks(EntityManagerInterface $manager, FashionWeeks $fashionWeeks)
    {
        $manager->remove($fashionWeeks);
        $manager->flush();

        $this->addFlash('success', "La fashion weeks a été supprimée avec succés !");

        return $this->redirectToRoute('admin_fashion_weeks');
    }

    /**
     * Méthode permettant l'affichage de la table SQL salon_type
     * 
     * @Route("/admin/salon_type", name="admin_salon_type")
     */
    public function adminSalonType(EntityManagerInterface $manager, SalonTypeRepository $repoSalonType): Response
    {
        $colonnes = $manager->getClassMetadata(SalonType::class)->getFieldNames();
        dump($colonnes);

        $salonType = $repoSalonType->findAll();
        dump($salonType);

        return $this->render('admin_calendar/admin_salon_type.html.twig', [
            'colonnes' => $colonnes,
            'salonType' => $salonType
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une fashion weeks
     * 
     * @Route("/admin/add_salon_type", name="add_salon_type")
     * @Route("/Admin/update_salon_type/{id}", name="update_salon_type")
     */
    public function adminFormSalonType(Request $request, EntityManagerInterface $manager, SalonType $salonType = null): Response 
    {
        if (!$salonType) {
            $salonType = new SalonType;
        }
        
        $formSalonType = $this->createForm(SalonFormType::class, $salonType);

        $formSalonType->handleRequest($request);

        dump($salonType);

        if($formSalonType->isSubmitted() && $formSalonType->isValid())
        {
            // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$salonType->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($salonType);
            $manager->flush();

            $this->addFlash('success', "La salon type a été $word avec succés !");

            return $this->redirectToRoute('admin_salon_type');
        }

        return $this->render('admin_calendar/admin_form_salon_type.html.twig', [
            'formSalonType' => $formSalonType->createView(),
            'editMode' => $salonType->getId()
        ]);
    }

    /**
     * Méthode permettant de supprimer une salon type week de la BDD
     * 
     * @Route("admin/delete_salon_type/{id}", name="delete_salon_type")
     */
    public function adminDeleteSalonType(EntityManagerInterface $manager, SalonType $salonType)
    {
        $manager->remove($salonType);
        $manager->flush();

        $this->addFlash('success', "La salon type a été supprimée avec succés !");

        return $this->redirectToRoute('admin_salon_type');
    }

    /**
     * Méthode permettant l'affichage de la table SQL saison
     * 
     * @Route("/admin/saisons_mode", name="admin_saisons_mode")
     */
    public function adminSaison(EntityManagerInterface $manager, SaisonsModeRepository $repoSaisonMode): Response
    {
        $colonnes = $manager->getClassMetadata(SaisonsMode::class)->getFieldNames();
        dump($colonnes);

        $saisonsMode = $repoSaisonMode->findAll();
        dump($saisonsMode);

        return $this->render('admin_calendar/admin_saisons_mode.html.twig', [
            'colonnes' => $colonnes,
            'saisonsMode' => $saisonsMode
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une saison mode
     * 
     * @Route("/admin/add_saison_mode", name="add_saison_mode")
     * @Route("/admin/update_saison_mode/{id}", name="update_saison_mode")
     */
    public function adminFormSaisonMode(Request $request, EntityManagerInterface $manager, SaisonsMode $saisonsMode = null): Response 
    {
        if (!$saisonsMode) {
            $saisonsMode = new SaisonsMode;
        }
        
        $formSaisonsMode = $this->createForm(SaisonsModeType::class, $saisonsMode);

        $formSaisonsMode->handleRequest($request);

        dump($saisonsMode);

        if($formSaisonsMode->isSubmitted() && $formSaisonsMode->isValid())
        {
            // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$saisonsMode->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($saisonsMode);
            $manager->flush();

            $this->addFlash('success', "La saison mode a été $word avec succés !");

            return $this->redirectToRoute('admin_saisons_mode');
        }

        return $this->render('admin_calendar/admin_form_saisons_mode.html.twig', [
            'formSaisonsMode' => $formSaisonsMode->createView(),
            'editMode' => $saisonsMode->getId()
        ]);
    }

    /**
     * Méthode permettant de supprimer une saison de la BDD
     * 
     * @Route("admin/delete_saison_mode/{id}", name="delete_saison_mode")
     */
    public function adminDeleteSaisonsMode(EntityManagerInterface $manager, SaisonsMode $saisonsMode)
    {
        $manager->remove($saisonsMode);
        $manager->flush();

        $this->addFlash('success', "La saison mode type a été supprimée avec succés !");

        return $this->redirectToRoute('admin_saisons_mode');
    }

    /**
     * Méthode permettant l'affichage de la table SQL salons
     * 
     * @Route("/admin/salons", name="admin_salons")
     */
    public function adminSalons(EntityManagerInterface $manager, SalonsRepository $repoSalons): Response
    {
        $colonnes = $manager->getClassMetadata(Salons::class)->getFieldNames();
        dump($colonnes);

        $salons = $repoSalons->findAll();
        dump($salons);

        return $this->render('admin_calendar/admin_salons.html.twig', [
            'colonnes' => $colonnes,
            'salons' => $salons
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une saison mode
     * 
     * @Route("/admin/add_salon", name="add_salon")
     * @Route("/admin/update_salon/{id}", name="update_salon")
     */
    public function adminFormSalons(Request $request, EntityManagerInterface $manager, Salons $salon = null): Response 
    {
        if (!$salon) {
            $salon = new Salons;
        }
        
        $formSalon = $this->createForm(SalonsFormType::class, $salon);

        $formSalon->handleRequest($request);

        dump($salon);

        if($formSalon->isSubmitted() && $formSalon->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$salon->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($salon);
            $manager->flush();

            $this->addFlash('success', "Le salon session a été $word avec succés !");

            return $this->redirectToRoute('admin_salons');
        }

        return $this->render('admin_calendar/admin_form_salon.html.twig', [
            'formSalon' => $formSalon->createView(),
            'editMode' => $salon->getId()
        ]);
    }

    /**
     * Méthode permettant de supprimer une saison de la BDD
     * 
     * @Route("admin/delete_salon/{id}", name="delete_salon")
     */
    public function adminDeleteSalons(EntityManagerInterface $manager, Salons $salon)
    {
        $manager->remove($salon);
        $manager->flush();

        $this->addFlash('success', "Le salon a été supprimée avec succés !");

        return $this->redirectToRoute('admin_salons');
    }

    /**
     * Méthode permettant l'affichage de la table SQL evenementsFashionWeeks
     * 
     * @Route("/admin/evenements_fws", name="admin_evenements_fws")
     */
    public function adminEventsFW(EntityManagerInterface $manager, EvenementFashionWeeksRepository $repoEvenementsFws): Response
    {
        $colonnes = $manager->getClassMetadata(EvenementFashionWeeks::class)->getFieldNames();
        dump($colonnes);

        $evenementsFws = $repoEvenementsFws->findAll();
        dump($evenementsFws);

        return $this->render('admin_calendar/admin_evenements_fws.html.twig', [
            'colonnes' => $colonnes,
            'evenementsFws' => $evenementsFws
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier un organisateur event
     * 
     * @Route("/admin/add_evenement_fws", name="add_evenement_fws")
     * @Route("/admin/update_evenement_fws/{id}", name="update_evenement_fws")
     */
    public function adminFormEvenementsFws(Request $request, EntityManagerInterface $manager, EvenementFashionWeeks $evenementFashionWeeks = null): Response 
    {
        if($evenementFashionWeeks)
        {
            // On conserve l'image d'origine en cas de modification, si nous souhaitons pas modifier l'image
            $imageActuelle = $evenementFashionWeeks->getImage();
        }

        if (!$evenementFashionWeeks) {
            $evenementFashionWeeks = new EvenementFashionWeeks;
        }
        
        $formEvenementFws = $this->createForm(EvenementFwsType::class, $evenementFashionWeeks);

        $formEvenementFws->handleRequest($request);

        dump($evenementFashionWeeks);

        if($formEvenementFws->isSubmitted() && $formEvenementFws->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$evenementFashionWeeks->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            // TRAITEMENT DE L'IMAGE
            $image = $formEvenementFws->get('image')->getData();
            dump($image);

            if($image)
            {
                // On récupère le nom d'origine du fichier 
                $nomOrigineFichier = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // dump($nomOrigineFichier);

                //                     jimi-hendrix       -   215184845884 .    jpg
                $nouveauNomFichier = $nomOrigineFichier . '-' . uniqid() . '.' . $image->guessExtension();
                dump($nouveauNomFichier);

                try{
                    // On copie l'image dans le bon dossier (images_directory service.yaml) 
                    $image->move(
                        $this->getParameter('images_directory'),
                        $nouveauNomFichier
                    );

                }catch(FileException $e){

                }

                // On enregistre l'image en BDD
                $evenementFashionWeeks->setImage($nouveauNomFichier);
            }
            else
            {
                dump($evenementFashionWeeks);
                $evenementFashionWeeks->setImage($imageActuelle);
            }

            $manager->persist($evenementFashionWeeks);
            $manager->flush();

            $this->addFlash('success', "L'évènement fashion week été $word avec succés !");

            return $this->redirectToRoute('admin_evenements_fws');
        }

        return $this->render('admin_calendar/admin_form_evenement_fws.html.twig', [
            'formEvenementFws' => $formEvenementFws->createView(),
            'editMode' => $evenementFashionWeeks->getId(),
            'image' => $evenementFashionWeeks->getImage()
        ]);
    }

    /**
     * Méthode permettant de supprimer un event fashion week de la BDD
     * 
     * @Route("admin/delete_evenement_fws/{id}", name="delete_evenement_fws")
     */
    public function adminDeleteEvenementFws(EntityManagerInterface $manager, EvenementFashionWeeks $EvenementFashionWeeks)
    {
        $manager->remove($EvenementFashionWeeks);
        $manager->flush();

        $this->addFlash('success', "L'évènement fashion week a été supprimée avec succés !");

        return $this->redirectToRoute('admin_evenements_fws');
    }

    /**
     * Méthode permettant l'affichage de la table SQL evenementsFashionWeeks
     * 
     * @Route("/admin/organisateur_event", name="admin_organisateur_event")
     */
    public function adminOrganisateurEvent(EntityManagerInterface $manager, OrganisateurEventRepository $repoOrganisateurEvent): Response
    {
        $colonnes = $manager->getClassMetadata(OrganisateurEvent::class)->getFieldNames();
        dump($colonnes);

        $OrganisateurEvent = $repoOrganisateurEvent->findAll();
        dump($OrganisateurEvent);

        return $this->render('admin_calendar/admin_organisateur_event.html.twig', [
            'colonnes' => $colonnes,
            'OrganisateurEvent' => $OrganisateurEvent
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier un organisateur event
     * 
     * @Route("/admin/add_organisateur_event", name="add_organisateur_event")
     * @Route("/admin/update_organisateur_event/{id}", name="update_organisateur_event")
     */
    public function adminFormOrganisateurEvent(Request $request, EntityManagerInterface $manager, OrganisateurEvent $organisateurEvent = null): Response 
    {
        if (!$organisateurEvent) {
            $organisateurEvent = new OrganisateurEvent;
        }
        
        $formOrganisateurEvent = $this->createForm(OrganisateurEventType::class, $organisateurEvent);

        $formOrganisateurEvent->handleRequest($request);

        dump($organisateurEvent);

        if($formOrganisateurEvent->isSubmitted() && $formOrganisateurEvent->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$organisateurEvent->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($organisateurEvent);
            $manager->flush();

            $this->addFlash('success', "L'organisateur event a été $word avec succés !");

            return $this->redirectToRoute('admin_organisateur_event');
        }

        return $this->render('admin_calendar/admin_form_organisateur_event.html.twig', [
            'formOrganisateurEvent' => $formOrganisateurEvent->createView(),
            'editMode' => $organisateurEvent->getId()
        ]);
    }

    /**
     * Méthode permettant de supprimer un organisateur event de la BDD
     * 
     * @Route("admin/delete_organisateur_event/{id}", name="delete_organisateur_event")
     */
    public function adminDeleteOrganisateurEvent(EntityManagerInterface $manager, OrganisateurEvent $organisateurEvent)
    {
        $manager->remove($organisateurEvent);
        $manager->flush();

        $this->addFlash('success', "L'organisateur event a été supprimée avec succés !");

        return $this->redirectToRoute('admin_organisateur_event');
    }

    /**
     * Méthode permettant l'affichage de la table SQL eventsFashionWeeks
     * 
     * @Route("/admin/events_fws", name="admin_events_fws")
     */
    public function adminEventsFws(EntityManagerInterface $manager, EventFashionWeekRepository $repoEventFws): Response
    {
        $colonnes = $manager->getClassMetadata(EventFashionWeek::class)->getFieldNames();
        dump($colonnes);

        $EventFws = $repoEventFws->findAll();
        dump($EventFws);

        return $this->render('admin_calendar/admin_event_fws.html.twig', [
            'colonnes' => $colonnes,
            'EventFws' => $EventFws
        ]);
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier un event fashion week
     * 
     * @Route("/admin/add_event_fsw", name="add_event_fsw")
     * @Route("/admin/update_event_fsw/{id}", name="update_event_fsw")
     */
    public function adminFormEventFws(Request $request, EntityManagerInterface $manager, EventFashionWeek $eventFashionWeek = null): Response 
    {
        if (!$eventFashionWeek) {
            $eventFashionWeek = new EventFashionWeek;
        }
        
        $formEventFws = $this->createForm(EventFwsType::class, $eventFashionWeek);

        $formEventFws->handleRequest($request);

        dump($eventFashionWeek);

        if($formEventFws->isSubmitted() && $formEventFws->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$eventFashionWeek->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($eventFashionWeek);
            $manager->flush();

            $this->addFlash('success', "L'event Fashion Week event a été $word avec succés !");

            return $this->redirectToRoute('admin_events_fws');
        }

        return $this->render('admin_calendar/admin_form_event_fws.html.twig', [
            'formEventFws' => $formEventFws->createView(),
            'editMode' => $eventFashionWeek->getId()
        ]);
    }

    /**
     * Méthode permettant de supprimer un event fashion week de la BDD
     * 
     * @Route("admin/delete_event_fws/{id}", name="delete_event_fws")
     */
    public function adminDeleteEventFws(EntityManagerInterface $manager, EventFashionWeek $EventFashionWeek)
    {
        $manager->remove($EventFashionWeek);
        $manager->flush();

        $this->addFlash('success', "L'event fashion week a été supprimée avec succés !");

        return $this->redirectToRoute('admin_events_fws');
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier un campaign_designer
     * 
     * @Route("/admin/add_campaign_designer", name="add_campaign_designer")
     * @Route("/admin/update_campaign_designer/{id}", name="update_campaign_designer")
     */
    public function adminFormCampaignDesigner(Request $request, EntityManagerInterface $manager, CampaignDesigner $campaignDesigner = null): Response 
    {
        if (!$campaignDesigner) {
            $campaignDesigner = new CampaignDesigner;
        }
        
        $formCampaignDesigner = $this->createForm(CampaignDesignerType::class, $campaignDesigner);

        $formCampaignDesigner->handleRequest($request);

        dump($campaignDesigner);

        if($formCampaignDesigner->isSubmitted() && $formCampaignDesigner->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$campaignDesigner->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($campaignDesigner);
            $manager->flush();

            $this->addFlash('success', "La campagne designer a été $word avec succés !");

            return $this->redirectToRoute('admin_campaign_designer');
        }

        return $this->render('admin_calendar/admin_form_campaign_designer.html.twig', [
            'formCampaignDesigner' => $formCampaignDesigner->createView(),
            'editMode' => $campaignDesigner->getId()
        ]);
    }

    /**
     * Méthode permettant l'affichage de la table SQL campaignDesigner
     * 
     * @Route("/admin/campaign_designer", name="admin_campaign_designer")
     */
    public function adminCampaignDesigner(EntityManagerInterface $manager, CampaignDesignerRepository $repoCampaignD): Response
    {
        $colonnes = $manager->getClassMetadata(CampaignDesigner::class)->getFieldNames();
        dump($colonnes);

        $CampaignD = $repoCampaignD->findAll();
        dump($CampaignD);

        return $this->render('admin_calendar/admin_campaign_designer.html.twig', [
            'colonnes' => $colonnes,
            'CampaignD' => $CampaignD
        ]);
    }

    /**
     * Méthode permettant de supprimer une campaigne Designer de la BDD
     * 
     * @Route("admin/delete_campaign_designer/{id}", name="delete_campaign_designer")
     */
    public function adminDeleteCampaignD(EntityManagerInterface $manager, CampaignDesigner $campaignD)
    {
        $manager->remove($campaignD);
        $manager->flush();

        $this->addFlash('success', "La campaign Designer a été supprimée avec succés !");

        return $this->redirectToRoute('admin_campaign_designer');
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier un campaign_designer
     * 
     * @Route("/admin/add_campaign_showroom", name="add_campaign_showroom")
     * @Route("/admin/update_campaign_showroom/{id}", name="update_campaign_showroom")
     */
    public function adminFormCampaignShowroom(Request $request, EntityManagerInterface $manager, CampaignShowroom $campaignShowroom = null): Response 
    {
        if (!$campaignShowroom) {
            $campaignShowroom = new CampaignShowroom;
        }
        
        $formCampaignShowroom = $this->createForm(CampaignShowroomType::class, $campaignShowroom);

        $formCampaignShowroom->handleRequest($request);

        dump($campaignShowroom);

        if($formCampaignShowroom->isSubmitted() && $formCampaignShowroom->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$campaignShowroom->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($campaignShowroom);
            $manager->flush();

            $this->addFlash('success', "La campagne showroom a été $word avec succés !");

            return $this->redirectToRoute('admin_campaign_showroom');
        }

        return $this->render('admin_calendar/admin_form_campaign_showroom.html.twig', [
            'formCampaignShowroom' => $formCampaignShowroom->createView(),
            'editMode' => $campaignShowroom->getId()
        ]);
    }

    /**
     * Méthode permettant l'affichage de la table SQL campaignshowroom
     * 
     * @Route("/admin/campaign_showroom", name="admin_campaign_showroom")
     */
    public function adminCampaignShowroom(EntityManagerInterface $manager, CampaignShowroomRepository $repoCampaignS): Response
    {
        $colonnes = $manager->getClassMetadata(CampaignShowroom::class)->getFieldNames();
        dump($colonnes);

        $CampaignS = $repoCampaignS->findAll();
        dump($CampaignS);

        return $this->render('admin_calendar/admin_campaign_showroom.html.twig', [
            'colonnes' => $colonnes,
            'CampaignS' => $CampaignS
        ]);
    }

    /**
     * Méthode permettant de supprimer une campaigne Showroom de la BDD
     * 
     * @Route("admin/delete_campaign_showroom/{id}", name="delete_campaign_showroom")
     */
    public function adminDeleteCampaignS(EntityManagerInterface $manager, CampaignShowroom $campaignS)
    {
        $manager->remove($campaignS);
        $manager->flush();

        $this->addFlash('success', "La campaign Showroom a été supprimée avec succés !");

        return $this->redirectToRoute('admin_campaign_showroom');
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une catégorie actu
     * 
     * @Route("/admin/add_category_actu", name="add_category_actu")
     * @Route("/admin/update_category_actu/{id}", name="update_category_actu")
     */
    public function adminFormCategoryActu(Request $request, EntityManagerInterface $manager, CategoryActu $categoryActu = null): Response 
    {
        if (!$categoryActu) {
            $categoryActu = new CategoryActu;
        }
        
        $formCategoryActu = $this->createForm(CategoryActuType::class, $categoryActu);

        $formCategoryActu->handleRequest($request);

        dump($categoryActu);

        if($formCategoryActu->isSubmitted() && $formCategoryActu->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$categoryActu->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($categoryActu);
            $manager->flush();

            $this->addFlash('success', "La catégorie actualité a été $word avec succés !");

            return $this->redirectToRoute('admin_category_actu');
        }

        return $this->render('admin_calendar/admin_form_category_actu.html.twig', [
            'formCategoryActu' => $formCategoryActu->createView(),
            'editMode' => $categoryActu->getId()
        ]);
    }

    /**
     * Méthode permettant l'affichage de la table SQL Categorie Actu
     * 
     * @Route("/admin/category_actu", name="admin_category_actu")
     */
    public function adminCategoryActu(EntityManagerInterface $manager, CategoryActuRepository $repoCategoryActu): Response
    {
        $colonnes = $manager->getClassMetadata(CategoryActu::class)->getFieldNames();
        dump($colonnes);

        $categoryActu = $repoCategoryActu->findAll();
        dump($categoryActu);

        return $this->render('admin_calendar/admin_category_actu.html.twig', [
            'colonnes' => $colonnes,
            'categoryActu' => $categoryActu
        ]);
    }

    /**
     * Méthode permettant de supprimer une catégorie actualité de la BDD
     * 
     * @Route("admin/delete_category_actu/{id}", name="delete_category_actu")
     */
    public function adminDeleteCategoryActu(EntityManagerInterface $manager, CategoryActu $categoryActu)
    {
        $manager->remove($categoryActu);
        $manager->flush();

        $this->addFlash('success', "La catégorie actualités a été supprimée avec succés !");

        return $this->redirectToRoute('admin_category_actu');
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une fil actu
     * 
     * @Route("/admin/add_fil_actu", name="add_fil_actu")
     * @Route("/admin/update_fil_actu/{id}", name="update_fil_actu")
     */
    public function adminFormFilActu(Request $request, EntityManagerInterface $manager, FilActu $filActu = null): Response 
    {
        if($filActu)
        {
            // On conserve l'image d'origine en cas de modification, si nous souhaitons pas modifier l'image
            $imageActuelle = $filActu->getImage();
        }

        if (!$filActu) {
            $filActu = new FilActu;
        }
        
        $formFilActu = $this->createForm(FilActuType::class, $filActu);

        $formFilActu->handleRequest($request);

        dump($filActu);

        if($formFilActu->isSubmitted() && $formFilActu->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$filActu->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            // TRAITEMENT DE L'IMAGE
            $image = $formFilActu->get('image')->getData();
            dump($image);

            if($image)
            {
                // On récupère le nom d'origine du fichier 
                $nomOrigineFichier = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // dump($nomOrigineFichier);

                //                     jimi-hendrix       -   215184845884 .    jpg
                $nouveauNomFichier = $nomOrigineFichier . '-' . uniqid() . '.' . $image->guessExtension();
                dump($nouveauNomFichier);

                try{
                    // On copie l'image dans le bon dossier (images_directory service.yaml) 
                    $image->move(
                        $this->getParameter('images_directory'),
                        $nouveauNomFichier
                    );

                }catch(FileException $e){

                }

                // On enregistre l'image en BDD
                $filActu->setImage($nouveauNomFichier);
            }
            else
            {
                dump($filActu);
                $filActu->setImage($imageActuelle);
            }

            $manager->persist($filActu);
            $manager->flush();

            $this->addFlash('success', "Le fil actualité a été $word avec succés !");

            return $this->redirectToRoute('admin_fil_actu');
        }

        return $this->render('admin_calendar/admin_form_fil_actu.html.twig', [
            'formFilActu' => $formFilActu->createView(),
            'editMode' => $filActu->getId(),
            'image' => $filActu->getImage()
        ]);
    }

    /**
     * Méthode permettant l'affichage de la table SQL fil Actu
     * 
     * @Route("/admin/fil_actu", name="admin_fil_actu")
     */
    public function adminFilActu(EntityManagerInterface $manager, FilActuRepository $repoFilActu): Response
    {
        $colonnes = $manager->getClassMetadata(FilActu::class)->getFieldNames();
        dump($colonnes);

        $filActu = $repoFilActu->findAll();
        dump($filActu);

        return $this->render('admin_calendar/admin_fil_actu.html.twig', [
            'colonnes' => $colonnes,
            'filActu' => $filActu
        ]);
    }

    /**
     * Méthode permettant de supprimer un fil actualité de la BDD
     * 
     * @Route("admin/delete_fil_actu/{id}", name="delete_fil_actu")
     */
    public function adminDeleteFilActu(EntityManagerInterface $manager, FilActu $filActu)
    {
        $manager->remove($filActu);
        $manager->flush();

        $this->addFlash('success', "Le fil a été supprimée avec succés !");

        return $this->redirectToRoute('admin_fil_actu');
    }

    /**
     * Méthode permettant à la fois d'ajouter mais aussi de modifier une saison sourcing
     * 
     * @Route("/admin/add_saison_sourcing", name="add_saison_sourcing")
     * @Route("/admin/update_saison_sourcing/{id}", name="update_saison_sourcing")
     */
    public function adminFormSaisonSourcing(Request $request, EntityManagerInterface $manager, SaisonSourcing $saisonSourcing = null): Response 
    {
        if (!$saisonSourcing) {
            $saisonSourcing = new SaisonSourcing;
        }
        
        $formSaisonSourcing = $this->createForm(SaisonSourcingType::class, $saisonSourcing);

        $formSaisonSourcing->handleRequest($request);

        dump($saisonSourcing);

        if($formSaisonSourcing->isSubmitted() && $formSaisonSourcing->isValid())
        {
            // // Si la catégorie en possède pas d'ID, c'est donc un insertion, on entre dans le IF
            if(!$saisonSourcing->getId())
                $word = 'enregistrée';
            else // Sinon la catégorie possède un ID, c'est donc une modification
                $word = 'modifiée';

            $manager->persist($saisonSourcing);
            $manager->flush();

            $this->addFlash('success', "La saison sourcing a été $word avec succés !");

            return $this->redirectToRoute('admin_saison_sourcing');
        }

        return $this->render('admin_calendar/admin_form_saison_sourcing.html.twig', [
            'formSaisonSourcing' => $formSaisonSourcing->createView(),
            'editMode' => $saisonSourcing->getId()
        ]);
    }

    /**
     * Méthode permettant l'affichage de la table SQL saison sourcing
     * 
     * @Route("/admin/saison_sourcing", name="admin_saison_sourcing")
     */
    public function adminSaisonSourcing(EntityManagerInterface $manager, SaisonSourcingRepository $repoSaisonSourcing): Response
    {
        $colonnes = $manager->getClassMetadata(SaisonSourcing::class)->getFieldNames();
        dump($colonnes);

        $saisonSourcing = $repoSaisonSourcing->findAll();
        dump($saisonSourcing);

        return $this->render('admin_calendar/admin_saison_sourcing.html.twig', [
            'colonnes' => $colonnes,
            'saisonSourcing' => $saisonSourcing
        ]);
    }

    /**
     * Méthode permettant de supprimer une saison sourcing de la BDD
     * 
     * @Route("admin/delete_saison_sourcing/{id}", name="delete_saison_sourcing")
     */
    public function adminDeleteSAisonSourcing(EntityManagerInterface $manager, SaisonSourcing $saisonSourcing)
    {
        $manager->remove($saisonSourcing);
        $manager->flush();

        $this->addFlash('success', "La saison sourcing a été supprimée avec succés !");

        return $this->redirectToRoute('admin_saison_sourcing');
    }
}
