<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\TokenService;
use App\Form\UserRegisterType;
use App\Service\MailerService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if(!empty($error)){
            $this -> addFlash('errors', 'Please check your credentials');
        }

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }


    /**
     * @Route("/login_check", name="app_login_check")
     */
    public function logingCheck()
    {
        
    }


    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager, TokenService $tokenService, MailerService $mailer, UserRepository $repo)
    {
        $user = new User; 
        $form = $this -> createForm(UserRegisterType::class, $user, []);
        $form -> handleRequest($request);

        if($form -> isSubmitted() && $form -> isValid()){
            $password = $user -> getPassword();
            $user 
                -> setPassword($encoder -> encodePassword($user, $password))
                -> setRole('ROLE_USER')
                -> setIsActive(true) 
                -> setIsConfirm(false) 
                -> setIsValidated(false)
                -> setToken($tokenService -> getRandomToken())
                -> setCreatedAt(new \Datetime('now'))
            ;
            
            $manager -> persist($user);
            $manager -> flush();

            $mailer -> sendConfirmAdressEmail($user);

            $this -> addFlash('success', 'You can signin now !');
            return $this -> redirectToRoute('app_login');
        }
        
        return $this -> render('security/register.html.twig', [
            'form' => $form -> createView(),
        ]);
        
    }


    
    /**
     * @Route("/confirm_account/{token}", name="app_register_confirm")
     */
    public function registerConfirm($token, UserRepository $repo, EntityManagerInterface $manager) : Response
    {
        $user = $repo -> findOneBy(['token' => $token]);
        if($user){
            if(!$user -> getIsConfirm()){
                $user 
                    -> setIsConfirm(true)
                    -> setConfirmAt(new \Datetime('now'))
                ;

                $manager -> persist($user);
                $manager -> flush();

                $this -> addFlash('success', 'Your account is now validated and ready');
                return $this -> redirectToRoute('app_login');
            }
            else{
                $this -> addFlash('errors', 'Account already validated');
                return $this -> redirectToRoute('home');
            }
        }
        else{
            $this -> addFlash('errors', 'Invalid account or token');
            return $this -> redirectToRoute('home');
        }
    }




    /**
     * 
     * 
     * @Route("test_token")
     */
    public function testToken(TokenService $tokenService){
       
        $tokenService -> getSmallRandomToken(10);
        
    }
}
