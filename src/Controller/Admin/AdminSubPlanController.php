<?php

namespace App\Controller\Admin;

use App\Entity\SubPlan;
use App\Form\SubPlanType;
use App\Service\TokenService;
use App\Repository\SubPlanRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("admin/subplan")
 */
class AdminSubPlanController extends AbstractController
{
    /**
     * @Route("/", name="admin_sub_plan_index", methods={"GET"})
     */
    public function index(SubPlanRepository $subPlanRepository): Response
    {
        return $this->render('admin/admin_sub_plan/index.html.twig', [
            'sub_plans' => $subPlanRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_sub_plan_new", methods={"GET","POST"})
     */
    public function new(Request $request, TokenService $tokenService): Response
    {
        $subPlan = new SubPlan();
        $form = $this->createForm(SubPlanType::class, $subPlan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($subPlan);
            $entityManager->flush();

            return $this->redirectToRoute('sub_plan_index');
        }

        return $this->render('admin/admin_sub_plan/new.html.twig', [
            'sub_plan' => $subPlan,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sub_plan_show", methods={"GET"})
     */
    public function show(SubPlan $subPlan): Response
    {
        return $this->render('admin/admin_sub_plan/show.html.twig', [
            'sub_plan' => $subPlan,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_sub_plan_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SubPlan $subPlan): Response
    {
        $form = $this->createForm(SubPlanType::class, $subPlan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_sub_plan_index');
        }

        return $this->render('admin/admin_sub_plan/edit.html.twig', [
            'sub_plan' => $subPlan,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sub_plan_delete", methods={"POST"})
     */
    public function delete(Request $request, SubPlan $subPlan): Response
    {
        if ($this->isCsrfTokenValid('delete'.$subPlan->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($subPlan);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_sub_plan_index');
    }
}
