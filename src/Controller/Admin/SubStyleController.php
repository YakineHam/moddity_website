<?php

namespace App\Controller\Admin;

use App\Entity\SubStyle;
use App\Form\SubStyleType;
use App\Repository\SubStyleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/sub/style")
 */
class SubStyleController extends AbstractController
{
    /**
     * @Route("/", name="admin_sub_style_index", methods={"GET"})
     */
    public function index(SubStyleRepository $subStyleRepository): Response
    {
        return $this->render('admin/sub_style/index.html.twig', [
            'sub_styles' => $subStyleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_sub_style_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $subStyle = new SubStyle();
        $form = $this->createForm(SubStyleType::class, $subStyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($subStyle);
            $entityManager->flush();

            return $this->redirectToRoute('admin_sub_style_index');
        }

        return $this->render('admin/sub_style/new.html.twig', [
            'sub_style' => $subStyle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sub_style_show", methods={"GET"})
     */
    public function show(SubStyle $subStyle): Response
    {
        return $this->render('admin/sub_style/show.html.twig', [
            'sub_style' => $subStyle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_sub_style_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SubStyle $subStyle): Response
    {
        $form = $this->createForm(SubStyleType::class, $subStyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_sub_style_index');
        }

        return $this->render('admin/sub_style/edit.html.twig', [
            'sub_style' => $subStyle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sub_style_delete", methods={"POST"})
     */
    public function delete(Request $request, SubStyle $subStyle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$subStyle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($subStyle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_sub_style_index');
    }
}
