<?php

namespace App\Controller\Admin;

use App\Entity\Collec;
use App\Entity\Collection;
use App\Form\CollectionType;
use App\Repository\CollecRepository;
use App\Repository\CollectionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/collection")
 */
class AdminCollectionController extends AbstractController
{
    /**
     * @Route("/", name="admin_collection_index", methods={"GET"})
     */
    public function index(CollecRepository $collectionRepository): Response
    {
        return $this->render('admin/admin_collection/index.html.twig', [
            'collections' => $collectionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_collection_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $collection = new Collec();
        $form = $this->createForm(CollectionType::class, $collection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($collection);
            $entityManager->flush();

            return $this->redirectToRoute('admin_collection_index');
        }

        return $this->render('admin/admin_collection/new.html.twig', [
            'collection' => $collection,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_collection_show", methods={"GET"})
     */
    public function show(Collec $collection): Response
    {
        return $this->render('admin/admin_collection/show.html.twig', [
            'collection' => $collection,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_collection_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Collec $collection): Response
    {
        $form = $this->createForm(CollectionType::class, $collection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_collection_index');
        }

        return $this->render('admin/admin_collection/edit.html.twig', [
            'collection' => $collection,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_collection_delete", methods={"POST"})
     */
    public function delete(Request $request, Collec $collection): Response
    {
        if ($this->isCsrfTokenValid('delete'.$collection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($collection);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_collection_index');
    }
}
