<?php

namespace App\Controller\Admin;

use App\Entity\External;
use App\Form\ExternalType;
use App\Repository\ExternalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/external")
 */
class AdminExternalController extends AbstractController
{
    /**
     * @Route("/", name="admin_external_index", methods={"GET"})
     */
    public function index(ExternalRepository $externalRepository): Response
    {
        return $this->render('admin/admin_external/index.html.twig', [
            'externals' => $externalRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_external_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $external = new External();
        $form = $this->createForm(ExternalType::class, $external);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($external);
            $entityManager->flush();

            return $this->redirectToRoute('admin_external_index');
        }

        return $this->render('admin/admin_external/new.html.twig', [
            'external' => $external,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_external_show", methods={"GET"})
     */
    public function show(External $external): Response
    {
        return $this->render('admin/admin_external/show.html.twig', [
            'external' => $external,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_external_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, External $external): Response
    {
        $form = $this->createForm(ExternalType::class, $external);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_external_index');
        }

        return $this->render('admin/admin_external/edit.html.twig', [
            'external' => $external,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_external_delete", methods={"POST"})
     */
    public function delete(Request $request, External $external): Response
    {
        if ($this->isCsrfTokenValid('delete'.$external->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($external);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_external_index');
    }
}
