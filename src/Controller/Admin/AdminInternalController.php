<?php

namespace App\Controller\Admin;

use App\Entity\Internal;
use App\Form\InternalType;
use App\Repository\InternalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/internal")
 */
class AdminInternalController extends AbstractController
{
    /**
     * @Route("/", name="admin_internal_index", methods={"GET"})
     */
    public function index(InternalRepository $internalRepository): Response
    {
        return $this->render('admin/admin_internal/index.html.twig', [
            'internals' => $internalRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_internal_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $internal = new Internal();
        $form = $this->createForm(InternalType::class, $internal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($internal);
            $entityManager->flush();

            return $this->redirectToRoute('admin_internal_index');
        }

        return $this->render('admin/admin_internal/new.html.twig', [
            'internal' => $internal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_internal_show", methods={"GET"})
     */
    public function show(Internal $internal): Response
    {
        return $this->render('admin/admin_internal/show.html.twig', [
            'internal' => $internal,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_internal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Internal $internal): Response
    {
        $form = $this->createForm(InternalType::class, $internal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_internal_index');
        }

        return $this->render('admin/admin_internal/edit.html.twig', [
            'internal' => $internal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_internal_delete", methods={"POST"})
     */
    public function delete(Request $request, Internal $internal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$internal->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($internal);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_internal_index');
    }
}
