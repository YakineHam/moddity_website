<?php

namespace App\Controller\Admin;

use App\Entity\Style;
use App\Form\StyleType;
use App\Repository\StyleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/style")
 */
class AdminStyleController extends AbstractController
{
    /**
     * @Route("/", name="admin_style_index", methods={"GET"})
     */
    public function index(StyleRepository $styleRepository): Response
    {
        return $this->render('admin/admin_style/index.html.twig', [
            'styles' => $styleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_style_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $style = new Style();
        $form = $this->createForm(StyleType::class, $style);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($style);
            $entityManager->flush();

            return $this->redirectToRoute('admin_style_index');
        }

        return $this->render('admin/admin_style/new.html.twig', [
            'style' => $style,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_style_show", methods={"GET"})
     */
    public function show(Style $style): Response
    {
        return $this->render('admin/admin_style/show.html.twig', [
            'style' => $style,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_style_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Style $style): Response
    {
        $form = $this->createForm(StyleType::class, $style);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_style_index');
        }

        return $this->render('admin/admin_style/edit.html.twig', [
            'style' => $style,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_style_delete", methods={"POST"})
     */
    public function delete(Request $request, Style $style): Response
    {
        if ($this->isCsrfTokenValid('delete'.$style->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($style);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_style_index');
    }
}
