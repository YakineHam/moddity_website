<?php

namespace App\Controller\Admin;

use App\Entity\Designer;
use App\Form\DesignerType;
use App\Repository\SubPlanRepository;
use App\Repository\DesignerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/designer")
 */
class AdminDesignerController extends AbstractController
{
    /**
     * @Route("/", name="admin_designer_index", methods={"GET"})
     */
    public function index(DesignerRepository $designerRepository): Response
    {
        return $this->render('admin_designer/index.html.twig', [
            'designers' => $designerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_designer_new", methods={"GET","POST"})
     */
    public function new(Request $request, SubPlanRepository $subPlanRepo): Response
    {
        $designer = new Designer();
        $sp_base = $subPlanRepo -> findOneBy(['uuid' => 0]);        
        $designer -> setSubPlan($sp_base);

        $form = $this->createForm(DesignerType::class, $designer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($designer);
            $entityManager->flush();

            return $this->redirectToRoute('admin_designer_index');
        }

        return $this->render('admin/admin_designer/new.html.twig', [
            'designer' => $designer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_designer_show", methods={"GET"})
     */
    public function show(Designer $designer): Response
    {
        return $this->render('admin_designer/show.html.twig', [
            'designer' => $designer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_designer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Designer $designer): Response
    {
        $form = $this->createForm(DesignerType::class, $designer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_designer_index');
        }

        return $this->render('admin_designer/edit.html.twig', [
            'designer' => $designer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_designer_delete", methods={"POST"})
     */
    public function delete(Request $request, Designer $designer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$designer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($designer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_designer_index');
    }
}
