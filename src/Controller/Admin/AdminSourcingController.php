<?php

namespace App\Controller\Admin;

use App\Entity\Sourcing;
use App\Form\SourcingType;
use App\Repository\SourcingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/sourcing")
 */
class AdminSourcingController extends AbstractController
{
    /**
     * @Route("/", name="admin_sourcing_index", methods={"GET"})
     */
    public function index(SourcingRepository $sourcingRepository): Response
    {
        return $this->render('admin/admin_sourcing/index.html.twig', [
            'sourcings' => $sourcingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_sourcing_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sourcing = new Sourcing();
        $form = $this->createForm(SourcingType::class, $sourcing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sourcing);
            $entityManager->flush();

            return $this->redirectToRoute('admin_sourcing_index');
        }

        return $this->render('admin/admin_sourcing/new.html.twig', [
            'sourcing' => $sourcing,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sourcing_show", methods={"GET"})
     */
    public function show(Sourcing $sourcing): Response
    {
        return $this->render('admin/admin_sourcing/show.html.twig', [
            'sourcing' => $sourcing,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_sourcing_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sourcing $sourcing): Response
    {
        $form = $this->createForm(SourcingType::class, $sourcing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_sourcing_index');
        }

        return $this->render('admin/admin_sourcing/edit.html.twig', [
            'sourcing' => $sourcing,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_sourcing_delete", methods={"POST"})
     */
    public function delete(Request $request, Sourcing $sourcing): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sourcing->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sourcing);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_sourcing_index');
    }
}
