<?php

namespace App\Controller\Admin;

use App\Entity\Land;
use App\Form\LandType;
use App\Repository\LandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/land")
 */
class AdminLandController extends AbstractController
{
    /**
     * @Route("/", name="admin_land_index", methods={"GET"})
     */
    public function index(LandRepository $landRepository): Response
    {
        return $this->render('admin/admin_land/index.html.twig', [
            'lands' => $landRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_land_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $land = new Land();
        $form = $this->createForm(LandType::class, $land);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($land);
            $entityManager->flush();

            return $this->redirectToRoute('admin_land_index');
        }

        return $this->render('admin/admin_land/new.html.twig', [
            'land' => $land,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_land_show", methods={"GET"})
     */
    public function show(Land $land): Response
    {
        return $this->render('admin/admin_land/show.html.twig', [
            'land' => $land,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_land_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Land $land): Response
    {
        $form = $this->createForm(LandType::class, $land);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_land_index');
        }

        return $this->render('admin/admin_land/edit.html.twig', [
            'land' => $land,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_land_delete", methods={"POST"})
     */
    public function delete(Request $request, Land $land): Response
    {
        if ($this->isCsrfTokenValid('delete'.$land->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($land);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_land_index');
    }
}
