<?php

namespace App\Controller\Admin;

use App\Entity\CompanySize;
use App\Form\CompanySizeType;
use App\Repository\CompanySizeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/company/size")
 */
class CompanySizeController extends AbstractController
{
    /**
     * @Route("/", name="company_size_index", methods={"GET"})
     */
    public function index(CompanySizeRepository $companySizeRepository): Response
    {
        return $this->render('admin/company_size/index.html.twig', [
            'company_sizes' => $companySizeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="company_size_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $companySize = new CompanySize();
        $form = $this->createForm(CompanySizeType::class, $companySize);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($companySize);
            $entityManager->flush();

            return $this->redirectToRoute('company_size_index');
        }

        return $this->render('admin/company_size/new.html.twig', [
            'company_size' => $companySize,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="company_size_show", methods={"GET"})
     */
    public function show(CompanySize $companySize): Response
    {
        return $this->render('admin/company_size/show.html.twig', [
            'company_size' => $companySize,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="company_size_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CompanySize $companySize): Response
    {
        $form = $this->createForm(CompanySizeType::class, $companySize);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('company_size_index');
        }

        return $this->render('admin/company_size/edit.html.twig', [
            'company_size' => $companySize,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="company_size_delete", methods={"POST"})
     */
    public function delete(Request $request, CompanySize $companySize): Response
    {
        if ($this->isCsrfTokenValid('delete'.$companySize->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($companySize);
            $entityManager->flush();
        }

        return $this->redirectToRoute('company_size_index');
    }
}
