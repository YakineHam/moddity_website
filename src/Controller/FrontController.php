<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
   
    /**
     * 
     * @Route("/articles", name="articles")
     * localhost:8000/articles
     * 
     */
    public function articlesShow() : Response
    {

        return $this -> render('front/articles.html.twig', [
            'pageName' => 'articles',
            'menuStyle' => 'articlesNoTitle'
        ]);

    }

    /**
     * 
     * @Route("/overview", name="overview")
     * localhost:8000/overview
     * 
     */
    public function overviewShow() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'overview',
            'menuStyle' => 'articlesLight'
        ]);

    }


    /**
     * 
     * @Route("/calendar/brand-sales", name="brand_sales")
     * 
     */
    public function calendarBrandSales() : Response
    {

        return $this -> render('front/calendar-sales.html.twig', [
            'pageName' => 'Brand Sales',
            'menuStyle' => 'calendar',
            'pageStyle' => 'overview'
        ]);

    }

    /**
     * 
     * @Route("/calendar/showroom-sales", name="showroom_sales")
     * 
     */
    public function showroomSales() : Response
    {

        return $this -> render('front/calendar-sales.html.twig', [
            'pageName' => 'Showrooms Sales',
            'menuStyle' => 'calendar',
            'pageStyle' => 'overview'
        ]);

    }


    /**
     * 
     * @Route("/calendar/shows", name="shows")
     * 
     */
    public function shows() : Response
    {

        return $this -> render('front/calendar-shows.html.twig', [
            'pageName' => 'Shows',
            'menuStyle' => 'calendar',
            'pageStyle' => 'overview'
        ]);

    }

    /**
     * 
     * @Route("/calendar/tradeshows", name="calendar_tradeshows")
     * 
     */
    public function calendarTradeshows() : Response
    {

        return $this -> render('front/calendar-shows.html.twig', [
            'pageName' => 'Tradeshows',
            'menuStyle' => 'calendar',
            'pageStyle' => 'overview'
        ]);

    }


    /**
     * 
     * @Route("/calendar/events", name="events")
     * 
     */
    public function events() : Response
    {

        return $this -> render('front/calendar-events.html.twig', [
            'pageName' => 'Events',
            'menuStyle' => 'calendar',
            'pageStyle' => 'overview'
        ]);

    }

    /**
     * 
     * @Route("/calendar/map", name="map")
     * 
     */
    public function map() : Response
    {

        return $this -> render('front/calendar-map.html.twig', [
            'pageName' => 'Map',
            'menuStyle' => 'calendar',
            'pageStyle' => 'overview',
            'pageTitle' => 'Map'
        ]);

    }

    /**
     * 
     * @Route("/stores", name="stores")
     * 
     */
    public function stores() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Stores',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Stores'
        ]);

    }

    /**
     * 
     * @Route("/showrooms-agents", name="showrooms_agents")
     * 
     */
    public function showroomsAgents() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Showrooms & Agents',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Showrooms & Agents'
        ]);

    }

    /**
     * 
     * @Route("/tradeshows", name="tradeshows")
     * 
     */
    public function tradeshows() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Tradeshows',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Tradeshows'
        ]);

    }

    /**
     * 
     * @Route("/services", name="services")
     * 
     */
    public function services() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Services',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Services'
        ]);

    }

    /**
     * 
     * @Route("/production", name="production")
     * 
     */
    public function production() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Production',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Production'
        ]);

    }

    /**
     * 
     * @Route("/brands", name="brands")
     * 
     */
    public function brands() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Brands Sales',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Brands Sales'
        ]);

    }

    /**
     * 
     * @Route("/communication", name="communication")
     * 
     */
    public function communication() : Response
    {

        return $this -> render('front/overview.html.twig', [
            'pageName' => 'Communication',
            'menuStyle' => 'articlesLight',
            'pageStyle' => 'overview',
            'pageTitle' => 'Communication'
        ]);

    }

    
    /**
     * 
     * @Route("/calendar", name="calendar")
     * 
     */
    public function calendar() : Response
    {

        return $this -> render('front/calendar.html.twig', [
            'pageName' => 'calendar',
            'menuStyle' => 'articlesNoTitle',
            'pageStyle' => 'overview',
            'pageTitle' => 'Calendar'
        ]);

    }


}
