<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminSecurityController extends AbstractController
{
    /**
     * @Route("/admin/security", name="admin_security")
     */
    public function index(): Response
    {
        return $this->render('admin_security/index.html.twig', [
            'controller_name' => 'AdminSecurityController',
        ]);
    }
}
