<?php

namespace App\Controller;

use App\Entity\User; 
use App\Service\TokenService;
use App\Form\UserRegisterType;
use App\Service\MailerService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin/user", name="admin_user")
     */
    public function adminUser(UserRepository $userRepo): Response
    {
        $users = $userRepo -> findBy([], ['createdAt' => 'DESC']);
        
        return $this -> render('admin_user/user_list.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/user/{id}/show", name="admin_user_show")
     */
    public function adminUserShow(User $user): Response
    {
        if($user){
            return $this -> render('admin_user/user_show.html.twig', [
                'user' => $user,
            ]);
        }
        else{
            throw new NotFoundException('User not found');
        }
    }

    /**
     * @Route("/admin/user/{id}/confirm", name="admin_user_confirm")
     */
    public function adminUserConfirm(User $user, EntityManagerInterface $manager, Request $request): Response
    {
        if($user){
            $user -> setIsConfirm(true);
            $user -> setConfirmAt(new \DateTime('now'));
    
            $manager -> persist($user);
            $manager -> flush();
    
            $this -> addFlash('success', sprintf('Le profil de l\'utilisateur <strong>%s</strong> a bien été mis à jour', $user -> getUsername()));
            return $this -> redirect($request -> headers -> get('referer'));
        }
        else{
            throw new NotFoundException('User not found');
        }
    }

    /**
     * @Route("/admin/user/{id}/validate", name="admin_user_validate")
     */
    public function adminUserValidate(User $user, EntityManagerInterface $manager, Request $request): Response
    {
        if($user){
            $user -> setIsValidated(true);
            $user -> setValidatedAt(new \DateTime('now'));

            $manager -> persist($user);
            $manager -> flush();
            $this -> addFlash('success', sprintf('Le profil de l\'utilisateur <strong>%s</strong> a bien été mis à jour', $user -> getUsername()));
            return $this -> redirect($request -> headers -> get('referer'));  
        }
        else{
            throw new NotFoundException('User not found');
        }
    }

    /**
     * @Route("/admin/user/{id}/delete", name="admin_user_delete")
     */
    public function adminUserDelete(User $user, EntityManagerInterface $manager, Request $request): Response
    {
        if($user){
            $user -> setIsActive(false);

            $manager -> persist($user);
            $manager -> flush();
            $this -> addFlash('success', sprintf('L\'utilisateur <strong>%s</strong> a bien été supprimé', $user -> getUsername()));
            return $this -> redirect($request -> headers -> get('referer'));  
        }
        else{
            throw new NotFoundException('User not found');
        }
    }

    /**
     * @Route("/admin/user/{id}/save", name="admin_user_save")
     */
    public function adminUserSave(User $user, EntityManagerInterface $manager, Request $request): Response
    {
        if($user){
            $user -> setIsActive(true);

            $manager -> persist($user);
            $manager -> flush();
            $this -> addFlash('success', sprintf('Le profil de l\'utilisateur <strong>%s</strong> a bien été mis à jour', $user -> getUsername()));
            return $this -> redirect($request -> headers -> get('referer'));  
        }
        else{
            throw new NotFoundException('User not found');
        }
    }

    /**
     * @Route("/admin/user/{id}/update", name="admin_user_update")
     */
    public function adminUserUpdate(User $user, EntityManagerInterface $manager, Request $request): Response
    {
        $action = 'modifier';
        $form = $this -> createForm(UserRegisterType::class, $user, [
            'admin' => $this -> getUser() -> getRole(),
            'current_action' => $action,
        ]);
        $form -> handleRequest($request);

        if($form -> isSubmitted() && $form -> isValid()){
            $manager -> persist($user);
            $manager -> flush();
            $this -> addFlash('success', sprintf('Le profil de l\'utilisateur <strong>%s</strong> a bien été mis à jour', $user -> getUsername()));
            return $this -> redirectToRoute('admin_user');  
        }

        return $this -> render('admin_user/user_form.html.twig', [
            'form' => $form -> createView(),
            'user' => $user,
            'action' => $action
        ]);
    }

    /**
     * @Route("/admin/user/add", name="admin_user_add")
     */
    public function adminUserAdd(UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager, Request $request, TokenService $tokenService, MailerService $mailer): Response
    {
        $action = 'ajouter';
        $user = new User();
        $form = $this -> createForm(UserRegisterType::class, $user, [
            'admin' => $this -> getUser() -> getRole(),
            'current_action' => $action
        ]);
        $form -> handleRequest($request);

        if($form -> isSubmitted() && $form -> isValid()){
            
            $password = $user -> getPassword();
            $user 
                -> setPassword($encoder -> encodePassword($user, $password))
                -> setIsActive(true) 
                -> setIsConfirm(true) 
                -> setIsValidated(true)
                -> setToken($tokenService -> getRandomToken())
                -> setCreatedAt(new \Datetime('now'))
            ;
            
            $manager -> persist($user);
            $manager -> flush();

            //$mailer -> sendConfirmAdressEmail($user);

            $this -> addFlash('success', sprintf('Le profil de l\'utilisateur <strong>%s</strong> a bien été mis à jour', $user -> getUsername()));
            return $this -> redirectToRoute('admin_user');  
        }

        return $this -> render('admin_user/user_form.html.twig', [
            'form' => $form -> createView(),
            'user' => $user,
            'action' => $action
        ]);
    }
}
