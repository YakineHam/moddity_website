<?php

namespace App\Controller;

use App\Entity\ProProfile;
use App\Form\ProProfileType;
use App\Repository\ProProfileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminProfileController extends AbstractController
{

      /**
     * @Route("/admin/", name="admin")
     */
    public function admin(): Response
    {

        return $this->render('admin_profile/index.html.twig', [
            
        ]);
    }


    /**
     * @Route("/admin/profiles/all", name="admin_profiles_all")
     */
    public function adminProfiles(ProProfileRepository $repo): Response
    {

        $profiles = $repo -> findBy(['type' => [
            'designer', 'boutique', 'showroom'
        ]]);

        return $this->render('admin_profile/profile_list.html.twig', [
            'profiles' => $profiles
        ]);
    }

    

    /**
     * @Route("/admin/profiles/{type}", name="admin_profiles_type")
     */
    public function adminProfilesType($type, ProProfileRepository $repo): Response
    {

        $profiles = $repo -> findBy(['type' => $type]);

        return $this->render('admin_profile/profile_list.html.twig', [
            'profiles' => $profiles
        ]);
    }


    /**
     * @Route("/admin/externals/all", name="admin_externals_all")
     */
    public function adminExternals(ProProfileRepository $repo): Response
    {

        $profiles = $repo -> findBy(['type' => [
            'communication', 'production', 'service'
        ]]);

        return $this->render('admin_profile/profile_list.html.twig', [
            'profiles' => $profiles
        ]);
    }

    /**
     * @Route("/admin/externals/{type}", name="admin_externals_type")
     */
    public function adminExternalsType($type, ProProfileRepository $repo): Response
    {

        $profiles = $repo -> findBy(['type' => $type]);

        return $this->render('admin_profile/profile_list.html.twig', [
            'profiles' => $profiles
        ]);
    }
    /**
     * @Route("/admin/profiles_add", name="admin_profiles_add")
     */
    public function adminProfileAdd(EntityManagerInterface $manager): Response
    {
        $pp = new ProProfile; 
        $form = $this -> createForm(ProProfileType::class, $pp,[]);
       
        return $this -> render('admin_profile/add_form.html.twig', [
            'form' => $form -> createView(),
        ]); 
    }
}
