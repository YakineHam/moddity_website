<?php

namespace App\Form;

use App\Entity\SalonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SalonFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'unique' => 'unique',
                    'multiple' => 'multiple',
                    ]
                ])
            ->add('choix')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SalonType::class,
        ]);
    }
}
