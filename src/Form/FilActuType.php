<?php

namespace App\Form;

use App\Entity\Salons;
use App\Entity\FilActu;
use App\Entity\CategoryActu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class FilActuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('salon', EntityType::class, [
                'class' => Salons::class, 
                'choice_label' => 'location' 
            ])
            ->add('categorieActu', EntityType::class, [
                'class' => CategoryActu::class, 
                'choice_label' => 'label' 
            ])
            ->add('saison')
            ->add('text_news')
            ->add('image', FileType::class, [
                'mapped' => true, // siginfie que le champ est associé à une propriété et qu'il sera inséré en BDD
                'required' => false,
                'data_class' => null,
                'constraints' => [
                    new File([
                        'maxSize' => '5M', // taille maximum d'upload d'image
                        'mimeTypes' => [ // format accepté
                            'image/jpeg',
                            'image/png',
                            'image/jpg'
                        ],
                        'mimeTypesMessage' => 'Formats autorisés : jpg/jpeg/png'
                    ])
                ]
            ])
            ->add('contact')
            ->add('publish_online')
            ->add('publish_direct')
            ->add('date_publication')
            ->add('publication_multiple')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FilActu::class,
        ]);
    }
}
