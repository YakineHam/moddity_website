<?php

namespace App\Form;

use App\Entity\FashionWeeks;
use App\Entity\EventFashionWeek;
use App\Entity\OrganisateurEvent;
use App\Entity\EvenementFashionWeeks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class EvenementFwsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fashionWeeks', EntityType::class, [
                'class' => FashionWeeks::class, 
                'choice_label' => 'name' 
            ])
            ->add('organisteurEvent',EntityType::class, [
                'class' => OrganisateurEvent::class, 
                'choice_label' => 'label' 
            ])
            ->add('EventFws', EntityType::class, [
                'class' => EventFashionWeek::class, 
                'choice_label' => 'label' 
            ])
            ->add('publish_map')
            ->add('date_unique')
            ->add('date_debut')
            ->add('date_fin')
            ->add('location')
            ->add('description')
            ->add('image', FileType::class, [
                'mapped' => true, // siginfie que le champ est associé à une propriété et qu'il sera inséré en BDD
                'required' => false,
                'data_class' => null,
                'constraints' => [
                    new File([
                        'maxSize' => '5M', // taille maximum d'upload d'image
                        'mimeTypes' => [ // format accepté
                            'image/jpeg',
                            'image/png',
                            'image/jpg'
                        ],
                        'mimeTypesMessage' => 'Formats autorisés : jpg/jpeg/png'
                    ])
                ]
            ])
            ->add('publish_online')
            ->add('adresse')
            ->add('ville')
            ->add('code_postal')
            ->add('nom')
            ->add('titre')
            ->add('telephone')
            ->add('email')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EvenementFashionWeeks::class,
        ]);
    }
}
