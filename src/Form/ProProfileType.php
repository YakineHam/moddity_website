<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\SubPlan;
use App\Entity\ProProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add('address')
            ->add('city')
            ->add('zipCode')
            ->add('siteUrl')
            ->add('emailMain')
            ->add('emailSale')
            ->add('emailCom')
            ->add('rsFb')
            ->add('rsTw')
            ->add('rsInsta')
            ->add('rsPint')
            ->add('headerImg')
            ->add('thumbnailsImg')
            ->add('logoImg')
            ->add('videoVid')
            ->add('lookookPdf')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Designer' => 'designer',
                    'Showroom' => 'showroom'
                ]
            ])
            ->add('latitude')
            ->add('longitude')
            ->add('subPlan', EntityType::class, [
                'class' => SubPlan::class,
                'choice_label' => 'label',
            ])
            ->add('collecs')
            ->add('styles')
            ->add('products')
            ->add('users', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'Username',
            ])
            ->add('token')
            ->add('shortToken')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProProfile::class,
        ]);
    }
}
