<?php

namespace App\Form;

use App\Entity\FashionWeeks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FashionWeeksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('place')
            ->add('start_date')
            ->add('end_date')
            ->add('section_online')
            ->add('creation_map')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FashionWeeks::class,
        ]);
    }
}
