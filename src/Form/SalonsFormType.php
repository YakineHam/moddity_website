<?php

namespace App\Form;

use App\Entity\Salons;
use App\Entity\SalonType;
use App\Entity\SaisonsMode;
use App\Entity\FashionWeeks;
use App\Repository\SalonTypeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SalonsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('fashion_weeks', EntityType::class, [
                'required'   => false,
                'class' => FashionWeeks::class, 
                'choice_label' => 'name' 
            ])
            ->add('salon_type', EntityType::class, [
                'class' => SalonType::class,
                'choice_label' => function (SalonType $salonType) {
                    return $salonType->getLabel() . ' - ' . $salonType->getType() . ' - ' . $salonType->getChoix();
                }
            ])
            ->add('saison_mode', EntityType::class, [
                'class' => SaisonsMode::class, 
                'choice_label' => 'label'
            ])
            ->add('start_date')
            ->add('end_date')
            ->add('location')
            ->add('address')
            ->add('city')
            ->add('zip_code')
            ->add('comments')
            ->add('link_salon_digital')
            ->add('link_exposants')
            ->add('publish_online')
            ->add('publish_map')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Salons::class,
        ]);
    }
}
