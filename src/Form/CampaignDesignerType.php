<?php

namespace App\Form;

use App\Entity\Collec;
use App\Entity\Salons;
use App\Entity\Product;
use App\Entity\Designer;
use App\Entity\SaisonsMode;
use App\Entity\FashionWeeks;
use App\Entity\CampaignDesigner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampaignDesignerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fashionWeeks', EntityType::class, [
                'class' => FashionWeeks::class, 
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('designer', EntityType::class, [
                'class' => Designer::class, 
                'choice_label' => 'label',
            ])
            ->add('saison', EntityType::class, [
                'class' => SaisonsMode::class, 
                'choice_label' => 'label' 
            ])
            ->add('salon', EntityType::class, [
                'class' => Salons::class, 
                'choice_label' => 'location' 
            ])
            ->add('collec', EntityType::class, [
                'class' => Collec::class, 
                'choice_label' => 'label' 
            ])
            ->add('products', EntityType::class, [
                'class' => Product::class, 
                'multiple' => true,
                'choice_label' => 'label' 
            ])
            ->add('publish_map')
            ->add('date_debut')
            ->add('date_fin')
            ->add('location')
            ->add('adresse')
            ->add('ville')
            ->add('code_postal')
            ->add('pays')
            ->add('commentaire')
            ->add('contact')
            ->add('publish_online')
            // ->add('calendrier')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CampaignDesigner::class,
        ]);
    }
}
