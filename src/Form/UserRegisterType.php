<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            -> add('lname', TextType::class, [
                'label' => 'Last name',
                'constraints' => [
                    new Assert\NotBlank(array()),
                    new Assert\Length(array(
                        'min' => 2,
                        'max' => 45,
                    )),
                ]
            ] )
            -> add('fname', TextType::class, [
                'label' => 'First name',
                'constraints' => [
                    new Assert\NotBlank(array()),
                    new Assert\Length(array(
                        'min' => 2,
                        'max' => 45,
                    )),
                ]
            ] )
            -> add('username', TextType::class, [
                'label' => 'Username',
                'constraints' => [
                    new Assert\NotBlank(array()),
                    new Assert\Length(array(
                        'min' => 2,
                        'max' => 45,
                    )),
                    new Assert\Regex(array(
                        'pattern' => '/[a-zA-Z0-9-_]{2,45}$/',
                    )),
                ]
            ])
            -> add('email', EmailType::class, [
                'label' => 'Email adress',
                'constraints' => [
                    new Assert\NotBlank(array()),
                    new Assert\Email(array()),
                ]
            ])
        ;

        if(!empty($options['admin'])){
            if($options['admin'] == 'ROLE_ADMIN'){
                $builder -> add('role', ChoiceType::class, [
                    'choices' => array(
                        'Visiteur' => 'ROLE_USER',
                        'Annonceur' => 'ROLE_CLIENT',
                        'Administrateur' => 'ROLE_ADMIN',
                    ),
                    'invalid_message' => 'The password fields must match.',
                    'required' => true,
                ]);
            }
            else if($options['admin'] == 'ROLE_SUPER_ADMIN'){
                $builder -> add('role', ChoiceType::class, [
                    'choices' => array(
                        'Visiteur' => 'ROLE_USER',
                        'Annonceur' => 'ROLE_CLIENT',
                        'Administrateur' => 'ROLE_ADMIN',
                        'Super Admin' => 'ROLE_SUPER_ADMIN',
                    ),
                    'invalid_message' => 'The password fields must match.',
                    'required' => true,
                ]);
            }

            if(!empty($options['current_action']) && $options['current_action'] == 'ajouter'){
                $builder -> add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'The password fields must match.',
                    'required' => true,
                    'first_options'  => ['label' => 'Password'],
                    'second_options' => ['label' => 'Confirm Password'],
                    'constraints' => [
                        new Assert\Regex(array(
                            'pattern' => '/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\-!?$#@\_])^[\w\d\-!?$#@]{8,20}$/',
                        )),
                    ]
                ]);
            }
        }
        else{
        $builder
            -> add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Confirm Password'],
                'constraints' => [
                    new Assert\Regex(array(
                        'pattern' => '/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\-!?$#@\_])^[\w\d\-!?$#@]{8,20}$/',
                    )),
                ]
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'admin' => '',
            'current_action' => '',
        ]);
    }
}
