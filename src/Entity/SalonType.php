<?php

namespace App\Entity;

use App\Repository\SalonTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SalonTypeRepository::class)
 */
class SalonType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $choix;

    /**
     * @ORM\OneToMany(targetEntity=Salons::class, mappedBy="salon_type", orphanRemoval=true)
     */
    private $salons;

    public function __construct()
    {
        $this->salons = new ArrayCollection();
        $this->campaignDesigners = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getChoix(): ?string
    {
        return $this->choix;
    }

    public function setChoix(string $choix): self
    {
        $this->choix = $choix;

        return $this;
    }

    /**
     * @return Collection|Salons[]
     */
    public function getSalons(): Collection
    {
        return $this->salons;
    }

    public function addSalon(Salons $salon): self
    {
        if (!$this->salons->contains($salon)) {
            $this->salons[] = $salon;
            $salon->setSalonType($this);
        }

        return $this;
    }

    public function removeSalon(Salons $salon): self
    {
        if ($this->salons->removeElement($salon)) {
            // set the owning side to null (unless already changed)
            if ($salon->getSalonType() === $this) {
                $salon->setSalonType(null);
            }
        }

        return $this;
    }
}
