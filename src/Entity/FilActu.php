<?php

namespace App\Entity;

use App\Repository\FilActuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FilActuRepository::class)
 */
class FilActu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=CategoryActu::class, inversedBy="filActus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorieActu;

    /**
     * @ORM\ManyToOne(targetEntity=Salons::class, inversedBy="filActus")
     */
    private $salon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $saison;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text_news;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="boolean")
     */
    private $publish_online;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $publish_direct;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_publication;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $publication_multiple;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategorieActu(): ?CategoryActu
    {
        return $this->categorieActu;
    }

    public function setCategorieActu(?CategoryActu $categorieActu): self
    {
        $this->categorieActu = $categorieActu;

        return $this;
    }

    public function getSalon(): ?Salons
    {
        return $this->salon;
    }

    public function setSalon(?Salons $salon): self
    {
        $this->salon = $salon;

        return $this;
    }

    public function getSaison(): ?string
    {
        return $this->saison;
    }

    public function setSaison(?string $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function getTextNews(): ?string
    {
        return $this->text_news;
    }

    public function setTextNews(?string $text_news): self
    {
        $this->text_news = $text_news;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getPublishOnline(): ?bool
    {
        return $this->publish_online;
    }

    public function setPublishOnline(bool $publish_online): self
    {
        $this->publish_online = $publish_online;

        return $this;
    }

    public function getPublishDirect(): ?bool
    {
        return $this->publish_direct;
    }

    public function setPublishDirect(?bool $publish_direct): self
    {
        $this->publish_direct = $publish_direct;

        return $this;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->date_publication;
    }

    public function setDatePublication(?\DateTimeInterface $date_publication): self
    {
        $this->date_publication = $date_publication;

        return $this;
    }

    public function getPublicationMultiple(): ?\DateTimeInterface
    {
        return $this->publication_multiple;
    }

    public function setPublicationMultiple(?\DateTimeInterface $publication_multiple): self
    {
        $this->publication_multiple = $publication_multiple;

        return $this;
    }
}
