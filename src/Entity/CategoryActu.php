<?php

namespace App\Entity;

use App\Repository\CategoryActuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryActuRepository::class)
 */
class CategoryActu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=FilActu::class, mappedBy="categorieActu", orphanRemoval=true)
     */
    private $filActus;

    public function __construct()
    {
        $this->filActus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|FilActu[]
     */
    public function getFilActus(): Collection
    {
        return $this->filActus;
    }

    public function addFilActu(FilActu $filActu): self
    {
        if (!$this->filActus->contains($filActu)) {
            $this->filActus[] = $filActu;
            $filActu->setCategorieActu($this);
        }

        return $this;
    }

    public function removeFilActu(FilActu $filActu): self
    {
        if ($this->filActus->removeElement($filActu)) {
            // set the owning side to null (unless already changed)
            if ($filActu->getCategorieActu() === $this) {
                $filActu->setCategorieActu(null);
            }
        }

        return $this;
    }
}
