<?php

namespace App\Entity;

use App\Repository\SaisonSourcingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaisonSourcingRepository::class)
 */
class SaisonSourcing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=EventProdServices::class, mappedBy="saison_sourcing")
     */
    private $eventProdServices;

    public function __construct()
    {
        $this->eventProdServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|EventProdServices[]
     */
    public function getEventProdServices(): Collection
    {
        return $this->eventProdServices;
    }

    public function addEventProdService(EventProdServices $eventProdService): self
    {
        if (!$this->eventProdServices->contains($eventProdService)) {
            $this->eventProdServices[] = $eventProdService;
            $eventProdService->setSaisonSourcing($this);
        }

        return $this;
    }

    public function removeEventProdService(EventProdServices $eventProdService): self
    {
        if ($this->eventProdServices->removeElement($eventProdService)) {
            // set the owning side to null (unless already changed)
            if ($eventProdService->getSaisonSourcing() === $this) {
                $eventProdService->setSaisonSourcing(null);
            }
        }

        return $this;
    }
}
