<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=145)
     */
    private $label;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity=CampaignDesigner::class, mappedBy="products")
     */
    private $campaignDesigners;

    public function __construct()
    {
        $this->campaignDesigners = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }



    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|CampaignDesigner[]
     */
    public function getCampaignDesigners(): Collection
    {
        return $this->campaignDesigners;
    }

    public function addCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if (!$this->campaignDesigners->contains($campaignDesigner)) {
            $this->campaignDesigners[] = $campaignDesigner;
            $campaignDesigner->addProduct($this);
        }

        return $this;
    }

    public function removeCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if ($this->campaignDesigners->removeElement($campaignDesigner)) {
            $campaignDesigner->removeProduct($this);
        }

        return $this;
    }
}
