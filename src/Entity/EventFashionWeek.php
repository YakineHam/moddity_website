<?php

namespace App\Entity;

use App\Repository\EventFashionWeekRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventFashionWeekRepository::class)
 */
class EventFashionWeek
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $physical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $digital;

    /**
     * @ORM\OneToMany(targetEntity=EvenementFashionWeeks::class, mappedBy="EventFws")
     */
    private $evenementFashionWeeks;

    public function __construct()
    {
        $this->evenementFashionWeeks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getPhysical(): ?bool
    {
        return $this->physical;
    }

    public function setPhysical(?bool $physical): self
    {
        $this->physical = $physical;

        return $this;
    }

    public function getDigital(): ?bool
    {
        return $this->digital;
    }

    public function setDigital(?bool $digital): self
    {
        $this->digital = $digital;

        return $this;
    }

    /**
     * @return Collection|EvenementFashionWeeks[]
     */
    public function getEvenementFashionWeeks(): Collection
    {
        return $this->evenementFashionWeeks;
    }

    public function addEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if (!$this->evenementFashionWeeks->contains($evenementFashionWeek)) {
            $this->evenementFashionWeeks[] = $evenementFashionWeek;
            $evenementFashionWeek->setEventFws($this);
        }

        return $this;
    }

    public function removeEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if ($this->evenementFashionWeeks->removeElement($evenementFashionWeek)) {
            // set the owning side to null (unless already changed)
            if ($evenementFashionWeek->getEventFws() === $this) {
                $evenementFashionWeek->setEventFws(null);
            }
        }

        return $this;
    }
}
