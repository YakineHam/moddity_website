<?php

namespace App\Entity;

use App\Repository\OrganisateurEventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrganisateurEventRepository::class)
 */
class OrganisateurEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=EvenementFashionWeeks::class, mappedBy="organisteurEvent", orphanRemoval=true)
     */
    private $evenementFashionWeeks;

    public function __construct()
    {
        $this->evenementFashionWeeks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|EvenementFashionWeeks[]
     */
    public function getEvenementFashionWeeks(): Collection
    {
        return $this->evenementFashionWeeks;
    }

    public function addEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if (!$this->evenementFashionWeeks->contains($evenementFashionWeek)) {
            $this->evenementFashionWeeks[] = $evenementFashionWeek;
            $evenementFashionWeek->setOrganisteurEvent($this);
        }

        return $this;
    }

    public function removeEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if ($this->evenementFashionWeeks->removeElement($evenementFashionWeek)) {
            // set the owning side to null (unless already changed)
            if ($evenementFashionWeek->getOrganisteurEvent() === $this) {
                $evenementFashionWeek->setOrganisteurEvent(null);
            }
        }

        return $this;
    }
}
