<?php

namespace App\Entity;

use App\Repository\CampaignShowroomRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CampaignShowroomRepository::class)
 */
class CampaignShowroom
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Calendar::class, inversedBy="campaignShowrooms")
     */
    private $calendrier;

    /**
     * @ORM\ManyToOne(targetEntity=FashionWeeks::class, inversedBy="campaignShowrooms")
     */
    private $fashionWeeks;

    /**
     * @ORM\ManyToOne(targetEntity=SaisonsMode::class, inversedBy="campaignShowrooms")
     */
    private $saison;

    /**
     * @ORM\ManyToOne(targetEntity=Salons::class, inversedBy="campaignShowrooms")
     */
    private $salon;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $publish_map;

    /**
     * @ORM\Column(type="date")
     */
    private $date_debut;

    /**
     * @ORM\Column(type="date")
     */
    private $date_fin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $publish_online;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalendrier(): ?Calendar
    {
        return $this->calendrier;
    }

    public function setCalendrier(?Calendar $calendrier): self
    {
        $this->calendrier = $calendrier;

        return $this;
    }

    public function getFashionWeeks(): ?FashionWeeks
    {
        return $this->fashionWeeks;
    }

    public function setFashionWeeks(?FashionWeeks $fashionWeeks): self
    {
        $this->fashionWeeks = $fashionWeeks;

        return $this;
    }

    public function getSaison(): ?SaisonsMode
    {
        return $this->saison;
    }

    public function setSaison(?SaisonsMode $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function getSalon(): ?Salons
    {
        return $this->salon;
    }

    public function setSalon(?Salons $salon): self
    {
        $this->salon = $salon;

        return $this;
    }

    public function getPublishMap(): ?bool
    {
        return $this->publish_map;
    }

    public function setPublishMap(?bool $publish_map): self
    {
        $this->publish_map = $publish_map;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getPublishOnline(): ?bool
    {
        return $this->publish_online;
    }

    public function setPublishOnline(?bool $publish_online): self
    {
        $this->publish_online = $publish_online;

        return $this;
    }
}
