<?php

namespace App\Entity;

use App\Repository\SaisonsModeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaisonsModeRepository::class)
 */
class SaisonsMode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Salons::class, mappedBy="saison_mode", orphanRemoval=true)
     */
    private $salons;

    /**
     * @ORM\OneToMany(targetEntity=CampaignDesigner::class, mappedBy="saison")
     */
    private $campaignDesigners;

    /**
     * @ORM\OneToMany(targetEntity=CampaignShowroom::class, mappedBy="saison")
     */
    private $campaignShowrooms;

    public function __construct()
    {
        $this->salons = new ArrayCollection();
        $this->campaignDesigners = new ArrayCollection();
        $this->campaignShowrooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Salons[]
     */
    public function getSalons(): Collection
    {
        return $this->salons;
    }

    public function addSalon(Salons $salon): self
    {
        if (!$this->salons->contains($salon)) {
            $this->salons[] = $salon;
            $salon->setSaisonMode($this);
        }

        return $this;
    }

    public function removeSalon(Salons $salon): self
    {
        if ($this->salons->removeElement($salon)) {
            // set the owning side to null (unless already changed)
            if ($salon->getSaisonMode() === $this) {
                $salon->setSaisonMode(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignDesigner[]
     */
    public function getCampaignDesigners(): Collection
    {
        return $this->campaignDesigners;
    }

    public function addCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if (!$this->campaignDesigners->contains($campaignDesigner)) {
            $this->campaignDesigners[] = $campaignDesigner;
            $campaignDesigner->setSaison($this);
        }

        return $this;
    }

    public function removeCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if ($this->campaignDesigners->removeElement($campaignDesigner)) {
            // set the owning side to null (unless already changed)
            if ($campaignDesigner->getSaison() === $this) {
                $campaignDesigner->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignShowroom[]
     */
    public function getCampaignShowrooms(): Collection
    {
        return $this->campaignShowrooms;
    }

    public function addCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if (!$this->campaignShowrooms->contains($campaignShowroom)) {
            $this->campaignShowrooms[] = $campaignShowroom;
            $campaignShowroom->setSaison($this);
        }

        return $this;
    }

    public function removeCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if ($this->campaignShowrooms->removeElement($campaignShowroom)) {
            // set the owning side to null (unless already changed)
            if ($campaignShowroom->getSaison() === $this) {
                $campaignShowroom->setSaison(null);
            }
        }

        return $this;
    }
}
