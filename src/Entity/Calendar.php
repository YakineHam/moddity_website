<?php

namespace App\Entity;

use App\Repository\CalendarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CalendarRepository::class)
 */
class Calendar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ProProfile::class, inversedBy="calendars")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proProfile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Salons::class, mappedBy="calendar", orphanRemoval=true)
     */
    private $salons;

    /**
     * @ORM\OneToMany(targetEntity=EvenementFashionWeeks::class, mappedBy="calendar", orphanRemoval=true)
     */
    private $evenementFashionWeeks;

    /**
     * @ORM\OneToMany(targetEntity=CampaignDesigner::class, mappedBy="calendrier")
     */
    private $campaignDesigners;

    /**
     * @ORM\OneToMany(targetEntity=CampaignShowroom::class, mappedBy="calendrier")
     */
    private $campaignShowrooms;

    public function __construct()
    {
        $this->salons = new ArrayCollection();
        $this->evenementFashionWeeks = new ArrayCollection();
        $this->campaignDesigners = new ArrayCollection();
        $this->campaignShowrooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProProfile(): ?ProProfile
    {
        return $this->proProfile;
    }

    public function setProProfile(?ProProfile $proProfile): self
    {
        $this->proProfile = $proProfile;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Salons[]
     */
    public function getSalons(): Collection
    {
        return $this->salons;
    }

    public function addSalon(Salons $salon): self
    {
        if (!$this->salons->contains($salon)) {
            $this->salons[] = $salon;
            $salon->setCalendar($this);
        }

        return $this;
    }

    public function removeSalon(Salons $salon): self
    {
        if ($this->salons->removeElement($salon)) {
            // set the owning side to null (unless already changed)
            if ($salon->getCalendar() === $this) {
                $salon->setCalendar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvenementFashionWeeks[]
     */
    public function getEvenementFashionWeeks(): Collection
    {
        return $this->evenementFashionWeeks;
    }

    public function addEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if (!$this->evenementFashionWeeks->contains($evenementFashionWeek)) {
            $this->evenementFashionWeeks[] = $evenementFashionWeek;
            $evenementFashionWeek->setCalendar($this);
        }

        return $this;
    }

    public function removeEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if ($this->evenementFashionWeeks->removeElement($evenementFashionWeek)) {
            // set the owning side to null (unless already changed)
            if ($evenementFashionWeek->getCalendar() === $this) {
                $evenementFashionWeek->setCalendar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignDesigner[]
     */
    public function getCampaignDesigners(): Collection
    {
        return $this->campaignDesigners;
    }

    public function addCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if (!$this->campaignDesigners->contains($campaignDesigner)) {
            $this->campaignDesigners[] = $campaignDesigner;
            $campaignDesigner->setCalendrier($this);
        }

        return $this;
    }

    public function removeCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if ($this->campaignDesigners->removeElement($campaignDesigner)) {
            // set the owning side to null (unless already changed)
            if ($campaignDesigner->getCalendrier() === $this) {
                $campaignDesigner->setCalendrier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignShowroom[]
     */
    public function getCampaignShowrooms(): Collection
    {
        return $this->campaignShowrooms;
    }

    public function addCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if (!$this->campaignShowrooms->contains($campaignShowroom)) {
            $this->campaignShowrooms[] = $campaignShowroom;
            $campaignShowroom->setCalendrier($this);
        }

        return $this;
    }

    public function removeCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if ($this->campaignShowrooms->removeElement($campaignShowroom)) {
            // set the owning side to null (unless already changed)
            if ($campaignShowroom->getCalendrier() === $this) {
                $campaignShowroom->setCalendrier(null);
            }
        }

        return $this;
    }
}
