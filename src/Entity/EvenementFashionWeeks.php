<?php

namespace App\Entity;

use App\Repository\EvenementFashionWeeksRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EvenementFashionWeeksRepository::class)
 */
class EvenementFashionWeeks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Calendar::class, inversedBy="evenementFashionWeeks")
     * @ORM\JoinColumn(nullable=true)
     */
    private $calendar;

    /**
     * @ORM\ManyToOne(targetEntity=FashionWeeks::class, inversedBy="evenementFashionWeeks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fashionWeeks;

    /**
     * @ORM\ManyToOne(targetEntity=OrganisateurEvent::class, inversedBy="evenementFashionWeeks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $organisteurEvent;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $publish_map;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_unique;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_debut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_fin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $publish_online;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=EventFashionWeek::class, inversedBy="evenementFashionWeeks")
     */
    private $EventFws;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalendar(): ?Calendar
    {
        return $this->calendar;
    }

    public function setCalendar(?Calendar $calendar): self
    {
        $this->calendar = $calendar;

        return $this;
    }

    public function getFashionWeeks(): ?FashionWeeks
    {
        return $this->fashionWeeks;
    }

    public function setFashionWeeks(?FashionWeeks $fashionWeeks): self
    {
        $this->fashionWeeks = $fashionWeeks;

        return $this;
    }

    public function getOrganisteurEvent(): ?OrganisateurEvent
    {
        return $this->organisteurEvent;
    }

    public function setOrganisteurEvent(?OrganisateurEvent $organisteurEvent): self
    {
        $this->organisteurEvent = $organisteurEvent;

        return $this;
    }

    public function getPublishMap(): ?bool
    {
        return $this->publish_map;
    }

    public function setPublishMap(?bool $publish_map): self
    {
        $this->publish_map = $publish_map;

        return $this;
    }

    public function getDateUnique(): ?\DateTimeInterface
    {
        return $this->date_unique;
    }

    public function setDateUnique(?\DateTimeInterface $date_unique): self
    {
        $this->date_unique = $date_unique;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(?\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(?\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPublishOnline(): ?bool
    {
        return $this->publish_online;
    }

    public function setPublishOnline(?bool $publish_online): self
    {
        $this->publish_online = $publish_online;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEventFws(): ?EventFashionWeek
    {
        return $this->EventFws;
    }

    public function setEventFws(?EventFashionWeek $EventFws): self
    {
        $this->EventFws = $EventFws;

        return $this;
    }
}
