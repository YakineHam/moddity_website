<?php

namespace App\Entity;

use App\Repository\SubPlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubPlanRepository::class)
 */
class SubPlan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=65)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $period;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="integer")
     */
    private $uuid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPaying;

    /**
     * @ORM\OneToMany(targetEntity=ProProfile::class, mappedBy="subPlan")
     */
    private $proProfiles;

    /**
     * @ORM\OneToMany(targetEntity=Designer::class, mappedBy="subPlan")
     */
    private $designers;

    public function __construct()
    {
        $this->proProfiles = new ArrayCollection();
        $this->designers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
    

    public function getUuid(): ?int
    {
        return $this->uuid;
    }

    public function setUuid(int $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getIsPaying(): ?bool
    {
        return $this->isPaying;
    }

    public function setIsPaying(bool $isPaying): self
    {
        $this->isPaying = $isPaying;

        return $this;
    }

    /**
     * @return Collection|ProProfile[]
     */
    public function getProProfiles(): Collection
    {
        return $this->proProfiles;
    }

    public function addProProfile(ProProfile $proProfile): self
    {
        if (!$this->proProfiles->contains($proProfile)) {
            $this->proProfiles[] = $proProfile;
            $proProfile->setSubPlan($this);
        }

        return $this;
    }

    public function removeProProfile(ProProfile $proProfile): self
    {
        if ($this->proProfiles->removeElement($proProfile)) {
            // set the owning side to null (unless already changed)
            if ($proProfile->getSubPlan() === $this) {
                $proProfile->setSubPlan(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Designer[]
     */
    public function getDesigners(): Collection
    {
        return $this->designers;
    }

    public function addDesigner(Designer $designer): self
    {
        if (!$this->designers->contains($designer)) {
            $this->designers[] = $designer;
            $designer->setSubPlan($this);
        }

        return $this;
    }

    public function removeDesigner(Designer $designer): self
    {
        if ($this->designers->removeElement($designer)) {
            // set the owning side to null (unless already changed)
            if ($designer->getSubPlan() === $this) {
                $designer->setSubPlan(null);
            }
        }

        return $this;
    }
}
