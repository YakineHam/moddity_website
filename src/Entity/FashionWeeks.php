<?php

namespace App\Entity;

use App\Repository\FashionWeeksRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FashionWeeksRepository::class)
 */
class FashionWeeks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place;

    /**
     * @ORM\Column(type="date")
     */
    private $start_date;

    /**
     * @ORM\Column(type="date")
     */
    private $end_date;

    /**
     * @ORM\Column(type="boolean")
     */
    private $section_online;

    /**
     * @ORM\Column(type="boolean")
     */
    private $creation_map;

    /**
     * @ORM\OneToMany(targetEntity=Salons::class, mappedBy="fashion_weeks")
     */
    private $salons;

    /**
     * @ORM\OneToMany(targetEntity=EvenementFashionWeeks::class, mappedBy="fashionWeeks", orphanRemoval=true)
     */
    private $evenementFashionWeeks;

    /**
     * @ORM\OneToMany(targetEntity=CampaignDesigner::class, mappedBy="fashionWeeks")
     */
    private $campaignDesigners;

    /**
     * @ORM\OneToMany(targetEntity=CampaignShowroom::class, mappedBy="fashionWeeks")
     */
    private $campaignShowrooms;

    public function __construct()
    {
        $this->salons = new ArrayCollection();
        $this->evenementFashionWeeks = new ArrayCollection();
        $this->campaignDesigners = new ArrayCollection();
        $this->campaignShowrooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getSectionOnline(): ?bool
    {
        return $this->section_online;
    }

    public function setSectionOnline(bool $section_online): self
    {
        $this->section_online = $section_online;

        return $this;
    }

    public function getCreationMap(): ?bool
    {
        return $this->creation_map;
    }

    public function setCreationMap(bool $creation_map): self
    {
        $this->creation_map = $creation_map;

        return $this;
    }

    /**
     * @return Collection|Salons[]
     */
    public function getSalons(): Collection
    {
        return $this->salons;
    }

    public function addSalon(Salons $salon): self
    {
        if (!$this->salons->contains($salon)) {
            $this->salons[] = $salon;
            $salon->setFashionWeeks($this);
        }

        return $this;
    }

    public function removeSalon(Salons $salon): self
    {
        if ($this->salons->removeElement($salon)) {
            // set the owning side to null (unless already changed)
            if ($salon->getFashionWeeks() === $this) {
                $salon->setFashionWeeks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvenementFashionWeeks[]
     */
    public function getEvenementFashionWeeks(): Collection
    {
        return $this->evenementFashionWeeks;
    }

    public function addEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if (!$this->evenementFashionWeeks->contains($evenementFashionWeek)) {
            $this->evenementFashionWeeks[] = $evenementFashionWeek;
            $evenementFashionWeek->setFashionWeeks($this);
        }

        return $this;
    }

    public function removeEvenementFashionWeek(EvenementFashionWeeks $evenementFashionWeek): self
    {
        if ($this->evenementFashionWeeks->removeElement($evenementFashionWeek)) {
            // set the owning side to null (unless already changed)
            if ($evenementFashionWeek->getFashionWeeks() === $this) {
                $evenementFashionWeek->setFashionWeeks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignDesigner[]
     */
    public function getCampaignDesigners(): Collection
    {
        return $this->campaignDesigners;
    }

    public function addCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if (!$this->campaignDesigners->contains($campaignDesigner)) {
            $this->campaignDesigners[] = $campaignDesigner;
            $campaignDesigner->setFashionWeeks($this);
        }

        return $this;
    }

    public function removeCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if ($this->campaignDesigners->removeElement($campaignDesigner)) {
            // set the owning side to null (unless already changed)
            if ($campaignDesigner->getFashionWeeks() === $this) {
                $campaignDesigner->setFashionWeeks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignShowroom[]
     */
    public function getCampaignShowrooms(): Collection
    {
        return $this->campaignShowrooms;
    }

    public function addCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if (!$this->campaignShowrooms->contains($campaignShowroom)) {
            $this->campaignShowrooms[] = $campaignShowroom;
            $campaignShowroom->setFashionWeeks($this);
        }

        return $this;
    }

    public function removeCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if ($this->campaignShowrooms->removeElement($campaignShowroom)) {
            // set the owning side to null (unless already changed)
            if ($campaignShowroom->getFashionWeeks() === $this) {
                $campaignShowroom->setFashionWeeks(null);
            }
        }

        return $this;
    }
}
