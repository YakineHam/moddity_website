<?php

namespace App\Entity;

use App\Repository\ProProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProProfileRepository::class)
 */
class ProProfile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=65)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $siteUrl;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $emailMain;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $emailSale;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $emailCom;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $rsFb;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $rsTw;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $rsInsta;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $rsPint;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $headerImg;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $thumbnailsImg;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $logoImg;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $videoVid;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lookookPdf;

    /**
     * @ORM\ManyToOne(targetEntity=SubPlan::class, inversedBy="proProfiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subPlan;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $type;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="proProfile", orphanRemoval=true)
     */
    private $photos;

    /**
     * @ORM\ManyToMany(targetEntity=Collec::class, mappedBy="proProfiles")
     */
    private $collecs;

    /**
     * @ORM\ManyToMany(targetEntity=Style::class, mappedBy="proProfiles")
     */
    private $styles;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="proProfiles")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $shortToken;

    /**
     * @ORM\OneToMany(targetEntity=Calendar::class, mappedBy="proProfile", orphanRemoval=true)
     */
    private $calendars;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->collections = new ArrayCollection();
        $this->collecs = new ArrayCollection();
        $this->styles = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->calendars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getSiteUrl(): ?string
    {
        return $this->siteUrl;
    }

    public function setSiteUrl(?string $siteUrl): self
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    public function getEmailMain(): ?string
    {
        return $this->emailMain;
    }

    public function setEmailMain(string $emailMain): self
    {
        $this->emailMain = $emailMain;

        return $this;
    }

    public function getEmailSale(): ?string
    {
        return $this->emailSale;
    }

    public function setEmailSale(?string $emailSale): self
    {
        $this->emailSale = $emailSale;

        return $this;
    }

    public function getEmailCom(): ?string
    {
        return $this->emailCom;
    }

    public function setEmailCom(?string $emailCom): self
    {
        $this->emailCom = $emailCom;

        return $this;
    }

    public function getRsFb(): ?string
    {
        return $this->rsFb;
    }

    public function setRsFb(?string $rsFb): self
    {
        $this->rsFb = $rsFb;

        return $this;
    }

    public function getRsTw(): ?string
    {
        return $this->rsTw;
    }

    public function setRsTw(?string $rsTw): self
    {
        $this->rsTw = $rsTw;

        return $this;
    }

    public function getRsInsta(): ?string
    {
        return $this->rsInsta;
    }

    public function setRsInsta(?string $rsInsta): self
    {
        $this->rsInsta = $rsInsta;

        return $this;
    }

    public function getRsPint(): ?string
    {
        return $this->rsPint;
    }

    public function setRsPint(?string $rsPint): self
    {
        $this->rsPint = $rsPint;

        return $this;
    }

    public function getHeaderImg(): ?string
    {
        return $this->headerImg;
    }

    public function setHeaderImg(?string $headerImg): self
    {
        $this->headerImg = $headerImg;

        return $this;
    }

    public function getThumbnailsImg(): ?string
    {
        return $this->thumbnailsImg;
    }

    public function setThumbnailsImg(?string $thumbnailsImg): self
    {
        $this->thumbnailsImg = $thumbnailsImg;

        return $this;
    }

    public function getLogoImg(): ?string
    {
        return $this->logoImg;
    }

    public function setLogoImg(?string $logoImg): self
    {
        $this->logoImg = $logoImg;

        return $this;
    }

    public function getVideoVid(): ?string
    {
        return $this->videoVid;
    }

    public function setVideoVid(?string $videoVid): self
    {
        $this->videoVid = $videoVid;

        return $this;
    }

    public function getLookookPdf(): ?string
    {
        return $this->lookookPdf;
    }

    public function setLookookPdf(?string $lookookPdf): self
    {
        $this->lookookPdf = $lookookPdf;

        return $this;
    }

    public function getSubPlan(): ?SubPlan
    {
        return $this->subPlan;
    }

    public function setSubPlan(?SubPlan $subPlan): self
    {
        $this->subPlan = $subPlan;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setProProfile($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getProProfile() === $this) {
                $photo->setProProfile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Collec[]
     */
    public function getCollecs(): Collection
    {
        return $this->collecs;
    }

    public function addCollec(Collec $collec): self
    {
        if (!$this->collecs->contains($collec)) {
            $this->collecs[] = $collec;
            $collec->addProProfile($this);
        }

        return $this;
    }

    public function removeCollec(Collec $collec): self
    {
        if ($this->collecs->removeElement($collec)) {
            $collec->removeProProfile($this);
        }

        return $this;
    }

    /**
     * @return Collection|Style[]
     */
    public function getStyles(): Collection
    {
        return $this->styles;
    }

    public function addStyle(Style $style): self
    {
        if (!$this->styles->contains($style)) {
            $this->styles[] = $style;
            $style->addProProfile($this);
        }

        return $this;
    }

    public function removeStyle(Style $style): self
    {
        if ($this->styles->removeElement($style)) {
            $style->removeProProfile($this);
        }

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addProProfile($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeProProfile($this);
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getShortToken(): ?string
    {
        return $this->shortToken;
    }

    public function setShortToken(string $shortToken): self
    {
        $this->shortToken = $shortToken;

        return $this;
    }

    /**
     * @return Collection|Calendar[]
     */
    public function getCalendars(): Collection
    {
        return $this->calendars;
    }

    public function addCalendar(Calendar $calendar): self
    {
        if (!$this->calendars->contains($calendar)) {
            $this->calendars[] = $calendar;
            $calendar->setProProfile($this);
        }

        return $this;
    }

    public function removeCalendar(Calendar $calendar): self
    {
        if ($this->calendars->removeElement($calendar)) {
            // set the owning side to null (unless already changed)
            if ($calendar->getProProfile() === $this) {
                $calendar->setProProfile(null);
            }
        }

        return $this;
    }

}
