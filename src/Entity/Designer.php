<?php

namespace App\Entity;

use App\Repository\DesignerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DesignerRepository::class)
 */
class Designer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $label;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=subPlan::class, inversedBy="designers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subPlan;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPulished;

    /**
     * @ORM\OneToMany(targetEntity=CampaignDesigner::class, mappedBy="designer", orphanRemoval=true)
     */
    private $campaignDesigners;

    public function __construct()
    {
        $this->campaignDesigners = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getSubPlan(): ?subPlan
    {
        return $this->subPlan;
    }

    public function setSubPlan(?subPlan $subPlan): self
    {
        $this->subPlan = $subPlan;

        return $this;
    }

    public function getIsPulished(): ?bool
    {
        return $this->isPulished;
    }

    public function setIsPulished(bool $isPulished): self
    {
        $this->isPulished = $isPulished;

        return $this;
    }

    /**
     * @return Collection|CampaignDesigner[]
     */
    public function getCampaignDesigners(): Collection
    {
        return $this->campaignDesigners;
    }

    public function addCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if (!$this->campaignDesigners->contains($campaignDesigner)) {
            $this->campaignDesigners[] = $campaignDesigner;
            $campaignDesigner->setDesigner($this);
        }

        return $this;
    }

    public function removeCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if ($this->campaignDesigners->removeElement($campaignDesigner)) {
            // set the owning side to null (unless already changed)
            if ($campaignDesigner->getDesigner() === $this) {
                $campaignDesigner->setDesigner(null);
            }
        }

        return $this;
    }
}
