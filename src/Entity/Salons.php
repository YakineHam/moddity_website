<?php

namespace App\Entity;

use App\Repository\SalonsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SalonsRepository::class)
 */
class Salons
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Calendar::class, inversedBy="salons")
     * @ORM\JoinColumn(nullable=true)
     */
    private $calendar;

    /**
     * @ORM\ManyToOne(targetEntity=FashionWeeks::class, inversedBy="salons")
     */
    private $fashion_weeks;

    /**
     * @ORM\ManyToOne(targetEntity=SalonType::class, inversedBy="salons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $salon_type;

    /**
     * @ORM\ManyToOne(targetEntity=SaisonsMode::class, inversedBy="salons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $saison_mode;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link_salon_digital;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link_exposants;

    /**
     * @ORM\Column(type="boolean")
     */
    private $publish_online;

    /**
     * @ORM\Column(type="boolean")
     */
    private $publish_map;

    /**
     * @ORM\OneToMany(targetEntity=CampaignDesigner::class, mappedBy="salon", orphanRemoval=true)
     */
    private $campaignDesigners;

    /**
     * @ORM\OneToMany(targetEntity=CampaignShowroom::class, mappedBy="salon")
     */
    private $campaignShowrooms;

    /**
     * @ORM\OneToMany(targetEntity=FilActu::class, mappedBy="salon")
     */
    private $filActus;

    /**
     * @ORM\OneToMany(targetEntity=EventProdServices::class, mappedBy="salon")
     */
    private $eventProdServices;

    public function __construct()
    {
        $this->campaignDesigners = new ArrayCollection();
        $this->campaignShowrooms = new ArrayCollection();
        $this->filActus = new ArrayCollection();
        $this->eventProdServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalendar(): ?Calendar
    {
        return $this->calendar;
    }

    public function setCalendar(?Calendar $calendar): self
    {
        $this->calendar = $calendar;

        return $this;
    }

    public function getFashionWeeks(): ?FashionWeeks
    {
        return $this->fashion_weeks;
    }

    public function setFashionWeeks(?FashionWeeks $fashion_weeks): self
    {
        $this->fashion_weeks = $fashion_weeks;

        return $this;
    }

    public function getSalonType(): ?SalonType
    {
        return $this->salon_type;
    }

    public function setSalonType(?SalonType $salon_type): self
    {
        $this->salon_type = $salon_type;

        return $this;
    }

    public function getSaisonMode(): ?SaisonsMode
    {
        return $this->saison_mode;
    }

    public function setSaisonMode(?SaisonsMode $saison_mode): self
    {
        $this->saison_mode = $saison_mode;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getLinkSalonDigital(): ?string
    {
        return $this->link_salon_digital;
    }

    public function setLinkSalonDigital(?string $link_salon_digital): self
    {
        $this->link_salon_digital = $link_salon_digital;

        return $this;
    }

    public function getLinkExposants(): ?string
    {
        return $this->link_exposants;
    }

    public function setLinkExposants(?string $link_exposants): self
    {
        $this->link_exposants = $link_exposants;

        return $this;
    }

    public function getPublishOnline(): ?bool
    {
        return $this->publish_online;
    }

    public function setPublishOnline(bool $publish_online): self
    {
        $this->publish_online = $publish_online;

        return $this;
    }

    public function getPublishMap(): ?bool
    {
        return $this->publish_map;
    }

    public function setPublishMap(bool $publish_map): self
    {
        $this->publish_map = $publish_map;

        return $this;
    }

    /**
     * @return Collection|CampaignDesigner[]
     */
    public function getCampaignDesigners(): Collection
    {
        return $this->campaignDesigners;
    }

    public function addCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if (!$this->campaignDesigners->contains($campaignDesigner)) {
            $this->campaignDesigners[] = $campaignDesigner;
            $campaignDesigner->setSalon($this);
        }

        return $this;
    }

    public function removeCampaignDesigner(CampaignDesigner $campaignDesigner): self
    {
        if ($this->campaignDesigners->removeElement($campaignDesigner)) {
            // set the owning side to null (unless already changed)
            if ($campaignDesigner->getSalon() === $this) {
                $campaignDesigner->setSalon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CampaignShowroom[]
     */
    public function getCampaignShowrooms(): Collection
    {
        return $this->campaignShowrooms;
    }

    public function addCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if (!$this->campaignShowrooms->contains($campaignShowroom)) {
            $this->campaignShowrooms[] = $campaignShowroom;
            $campaignShowroom->setSalon($this);
        }

        return $this;
    }

    public function removeCampaignShowroom(CampaignShowroom $campaignShowroom): self
    {
        if ($this->campaignShowrooms->removeElement($campaignShowroom)) {
            // set the owning side to null (unless already changed)
            if ($campaignShowroom->getSalon() === $this) {
                $campaignShowroom->setSalon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FilActu[]
     */
    public function getFilActus(): Collection
    {
        return $this->filActus;
    }

    public function addFilActu(FilActu $filActu): self
    {
        if (!$this->filActus->contains($filActu)) {
            $this->filActus[] = $filActu;
            $filActu->setSalon($this);
        }

        return $this;
    }

    public function removeFilActu(FilActu $filActu): self
    {
        if ($this->filActus->removeElement($filActu)) {
            // set the owning side to null (unless already changed)
            if ($filActu->getSalon() === $this) {
                $filActu->setSalon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventProdServices[]
     */
    public function getEventProdServices(): Collection
    {
        return $this->eventProdServices;
    }

    public function addEventProdService(EventProdServices $eventProdService): self
    {
        if (!$this->eventProdServices->contains($eventProdService)) {
            $this->eventProdServices[] = $eventProdService;
            $eventProdService->setSalon($this);
        }

        return $this;
    }

    public function removeEventProdService(EventProdServices $eventProdService): self
    {
        if ($this->eventProdServices->removeElement($eventProdService)) {
            // set the owning side to null (unless already changed)
            if ($eventProdService->getSalon() === $this) {
                $eventProdService->setSalon(null);
            }
        }

        return $this;
    }
}
