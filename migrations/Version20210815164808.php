<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210815164808 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE collec (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(145) NOT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE collec_pro_profile (collec_id INT NOT NULL, pro_profile_id INT NOT NULL, INDEX IDX_AC48437D584D4E9A (collec_id), INDEX IDX_AC48437D7F8CA318 (pro_profile_id), PRIMARY KEY(collec_id, pro_profile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(145) NOT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_pro_profile (product_id INT NOT NULL, pro_profile_id INT NOT NULL, INDEX IDX_D8D1F2CB4584665A (product_id), INDEX IDX_D8D1F2CB7F8CA318 (pro_profile_id), PRIMARY KEY(product_id, pro_profile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE style (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(145) NOT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE style_pro_profile (style_id INT NOT NULL, pro_profile_id INT NOT NULL, INDEX IDX_61DA391BBACD6074 (style_id), INDEX IDX_61DA391B7F8CA318 (pro_profile_id), PRIMARY KEY(style_id, pro_profile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_pro_profile (user_id INT NOT NULL, pro_profile_id INT NOT NULL, INDEX IDX_DDE5A541A76ED395 (user_id), INDEX IDX_DDE5A5417F8CA318 (pro_profile_id), PRIMARY KEY(user_id, pro_profile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE collec_pro_profile ADD CONSTRAINT FK_AC48437D584D4E9A FOREIGN KEY (collec_id) REFERENCES collec (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE collec_pro_profile ADD CONSTRAINT FK_AC48437D7F8CA318 FOREIGN KEY (pro_profile_id) REFERENCES pro_profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_pro_profile ADD CONSTRAINT FK_D8D1F2CB4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_pro_profile ADD CONSTRAINT FK_D8D1F2CB7F8CA318 FOREIGN KEY (pro_profile_id) REFERENCES pro_profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE style_pro_profile ADD CONSTRAINT FK_61DA391BBACD6074 FOREIGN KEY (style_id) REFERENCES style (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE style_pro_profile ADD CONSTRAINT FK_61DA391B7F8CA318 FOREIGN KEY (pro_profile_id) REFERENCES pro_profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_pro_profile ADD CONSTRAINT FK_DDE5A541A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_pro_profile ADD CONSTRAINT FK_DDE5A5417F8CA318 FOREIGN KEY (pro_profile_id) REFERENCES pro_profile (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE collec_pro_profile DROP FOREIGN KEY FK_AC48437D584D4E9A');
        $this->addSql('ALTER TABLE product_pro_profile DROP FOREIGN KEY FK_D8D1F2CB4584665A');
        $this->addSql('ALTER TABLE style_pro_profile DROP FOREIGN KEY FK_61DA391BBACD6074');
        $this->addSql('DROP TABLE collec');
        $this->addSql('DROP TABLE collec_pro_profile');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_pro_profile');
        $this->addSql('DROP TABLE style');
        $this->addSql('DROP TABLE style_pro_profile');
        $this->addSql('DROP TABLE user_pro_profile');
    }
}
