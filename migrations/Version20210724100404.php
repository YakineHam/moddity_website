<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210724100404 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photo ADD pro_profile_id INT NOT NULL');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B784187F8CA318 FOREIGN KEY (pro_profile_id) REFERENCES pro_profile (id)');
        $this->addSql('CREATE INDEX IDX_14B784187F8CA318 ON photo (pro_profile_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B784187F8CA318');
        $this->addSql('DROP INDEX IDX_14B784187F8CA318 ON photo');
        $this->addSql('ALTER TABLE photo DROP pro_profile_id');
    }
}
