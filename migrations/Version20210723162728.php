<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723162728 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE designer (id INT AUTO_INCREMENT NOT NULL, sub_plan_id INT DEFAULT NULL, label VARCHAR(150) NOT NULL, address VARCHAR(100) DEFAULT NULL, city VARCHAR(65) NOT NULL, zip_code VARCHAR(15) NOT NULL, site_url VARCHAR(150) DEFAULT NULL, email_main VARCHAR(150) NOT NULL, email_sale VARCHAR(150) DEFAULT NULL, email_com VARCHAR(150) DEFAULT NULL, rs_fb VARCHAR(150) DEFAULT NULL, rs_tw VARCHAR(150) DEFAULT NULL, rs_insta VARCHAR(150) DEFAULT NULL, rs_pint VARCHAR(150) DEFAULT NULL, header_img VARCHAR(50) DEFAULT NULL, thumbnails_img VARCHAR(50) DEFAULT NULL, logo_img VARCHAR(50) DEFAULT NULL, video_vid VARCHAR(50) DEFAULT NULL, lookook_pdf VARCHAR(50) DEFAULT NULL, INDEX IDX_B3A0DE5B97415E6F (sub_plan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_plan (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(65) NOT NULL, period VARCHAR(15) NOT NULL, price DOUBLE PRECISION NOT NULL, is_active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE designer ADD CONSTRAINT FK_B3A0DE5B97415E6F FOREIGN KEY (sub_plan_id) REFERENCES sub_plan (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE designer DROP FOREIGN KEY FK_B3A0DE5B97415E6F');
        $this->addSql('DROP TABLE designer');
        $this->addSql('DROP TABLE sub_plan');
    }
}
