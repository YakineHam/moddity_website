$(function(){  

    /**
     * 
     * Hide or show the password 
     * 
     */
    $('.eyeShow').on('click', function(){
        let input = $(this).parent().parent().parent().find('input');
        if(input.attr('data-shown') == '1'){
            input.attr('type', 'password');
            input.attr('data-shown', '0');
        }
        else{
            input.attr('type', 'text');
            input.attr('data-shown', '1');
        }
        
        $(this).toggleClass('fa-eye').toggleClass('fa-eye-slash');
    });


    /**
     * 
     * Fonction for the label top effect on custom form/label
     * 
     */
    $('.mc-form-control').each(function(){
        console.log($(this).val().length);
        if($(this).val().length > 0){
            console.log('ya qqchose')
            $(this).next('.mc-label').addClass('mc-label-fixed');
        }
    });


    $('.mc-form-control').on('blur', function(){
        if($(this).val().length > 0){
            $(this).next('.mc-label').addClass('mc-label-fixed');
        }
        else{
            $(this).next('.mc-label').removeClass('mc-label-fixed');
        }
    })

    $('.mc-label').on('click', function(){
        let field = $(this).parent().find('.mc-form-control');
        field.focus();
    });


  

});