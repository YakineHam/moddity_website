/*************************************************************************************/
// -->Template Name: Bootstrap Press Admin
// -->Author: Themedesigner
// -->Email: niravjoshi87@gmail.com
// -->File: datatable_basic_init
/*************************************************************************************/

$(document).ready(function() {
    /****************************************
     *       Basic Table                   *
     ****************************************/
    $('#table-salons').DataTable();

    /****************************************
     *       Default Order Table           *
     ****************************************/
    // $('#table-salons').DataTable({
    //     "order": [
    //         [3, "desc"]
    //     ]
    // });

    /****************************************
     *       Multi-column Order Table      *
     ****************************************/
    // $('#table-salons').DataTable({
    //     columnDefs: [{
    //         targets: [0],
    //         orderData: [0, 1]
    //     }, {
    //         targets: [1],
    //         orderData: [1, 0]
    //     }, {
    //         targets: [4],
    //         orderData: [4, 0]
    //     }]
    // });

});